import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ClientFormService {
  constructor(private readonly http: HttpClient) {}

  submitQueryForm(
    response_name,
    response_email,
    response_phoneNumber,
    response_organizationType,
    response_team,
    response_queryType,
    response_priority,
    response_queryBrief,
  ) {
    const url = '/api/';

    const clientEnquiry = {
      name: response_name,
      email: response_email,
      phoneNumber: response_phoneNumber,
      organizationType: response_organizationType,
      team: response_team,
      queryType: response_queryType,
      priority: response_priority,
      queryBrief: response_queryBrief,
    };

    return this.http.post(url + 'enquiry/v1/create', { body: clientEnquiry });
  }
}
