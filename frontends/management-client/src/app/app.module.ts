import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountingUIModule } from './accounting-ui/accounting-ui.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { SharedImportsModule } from './shared-imports/shared-imports.module';
import { AuthGuard } from './common/guards/auth.guard';
import { AppService } from './app.service';
import { HttpErrorHandler } from './http-error-handler.service';
import { MessageService } from './message.service';
import { ListingService } from './common/services/listing-service/listing.service';
import { TreeService } from './accounting-ui/tree/tree.service';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { InvoicingUIModule } from './invoicing-ui/invoicing-ui.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AccountingUIModule,
    SharedImportsModule,
    SharedUIModule,
    InvoicingUIModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    ScrollingModule,
    FormsModule,
    OAuthModule.forRoot(),
    FlexLayoutModule,
  ],
  providers: [
    AuthGuard,
    AppService,
    HttpErrorHandler,
    MessageService,
    ListingService,
    { provide: OAuthStorage, useValue: localStorage },
    TreeService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
