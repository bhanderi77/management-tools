import { Component, ViewChild } from '@angular/core';
import {
  ACCOUNTING_SERVER,
  CLOSE,
  ACCOUNT_LISTING_COLUMN,
  JOURNAL_ENTRY_LISTING,
  INVOICE_LISTING,
  PURCHASE_INVOICE_MODEL,
  SALES_INVOICE_MODEL,
  INVOICING_SERVER,
  SELECTED_INVOICE_SERVER,
} from '../../constants/storage';
import { MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { ListingDataSource } from './listing-datasource';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListingService } from '../../common/services/listing-service/listing.service';
import { StorageService } from '../../common/services/storage-service/storage.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  dataSource: ListingDataSource;
  displayedColumns: string[];
  domainList: any[];
  domain: string;
  model: string;
  search: string = '';
  accountNumber: string;
  accountName: string;
  accountType: string;
  isGroup: boolean;
  accountForm: FormGroup;
  selectedDomain: any = {};
  constructor(
    private listingService: ListingService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly storageService: StorageService,
    private readonly snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
  ) {
    this.dataSource = new ListingDataSource(this.model, this.listingService);
    this.accountForm = this.formBuilder.group({
      domain: '',
    });
    this.activatedRoute.params.subscribe({
      next: route => {
        this.model = this.activatedRoute.snapshot.params.listingModel;
        this.domainList = this.storageService.getDomainList(ACCOUNTING_SERVER);
        this.dataSource = new ListingDataSource(
          this.model,
          this.listingService,
        );
        if (this.model === 'account') {
          this.displayedColumns = ACCOUNT_LISTING_COLUMN;
          this.selectedDomain = this.storageService.getSelectedDomain();
          this.domainList = this.storageService.getDomainList(
            ACCOUNTING_SERVER,
          );
          this.dataSource.loadItems();
        }
        if (this.model === 'journalentry') {
          this.displayedColumns = JOURNAL_ENTRY_LISTING;
          this.selectedDomain = this.storageService.getSelectedDomain();
          this.domainList = this.storageService.getDomainList(
            ACCOUNTING_SERVER,
          );
          this.dataSource.loadItems();
        }
        if (
          this.model === SALES_INVOICE_MODEL ||
          this.model === PURCHASE_INVOICE_MODEL
        ) {
          this.selectedDomain = this.storageService.getSelectedDomain(
            this.model,
          );
          this.displayedColumns = INVOICE_LISTING;
          this.domainList = this.storageService.getDomainList(INVOICING_SERVER);
          this.dataSource.loadItems();
        }
      },
    });
  }

  getUpdate(event: MatPaginator) {
    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      event.pageIndex,
      event.pageSize,
    );
  }

  setFilter() {
    this.dataSource.loadItems(
      this.search, // TODO: Filter
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }

  setColumnName(camelCase) {
    const result = camelCase.replace(/([A-Z])/g, ' $1');
    return result.charAt(0).toUpperCase() + result.slice(1);
  }

  async getDomainData() {
    const url = this.accountForm.controls.domain.value;
    if (!url) {
      this.snackBar.open('please Select a Domain', CLOSE, { duration: 3500 });
      return;
    }
    this.selectedDomain = url;
    if (
      this.model === SALES_INVOICE_MODEL ||
      this.model === PURCHASE_INVOICE_MODEL
    ) {
      await this.storageService.setSelectedDomain(url, SELECTED_INVOICE_SERVER);
    } else {
      await this.storageService.setSelectedDomain(url);
    }
    this.snackBar.open('Domain Set To ' + url.name, CLOSE, { duration: 2500 });
    this.dataSource.loadItems();
  }

  formatRowData(data) {
    if (typeof data === 'boolean') {
      if (data) return '✓';
      else return '';
    }
    return data;
  }
}
