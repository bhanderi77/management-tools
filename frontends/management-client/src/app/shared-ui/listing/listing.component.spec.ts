import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ListingComponent } from './listing.component';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListingService } from '../../common/services/listing-service/listing.service';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { MaterialModule } from '../../shared-imports/material/material.module';

const listingService: Partial<ListingService> = {
  findModels: (...args) => of([]),
};

describe('ListingComponent', () => {
  let component: ListingComponent;
  let fixture: ComponentFixture<ListingComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListingComponent],
      imports: [
        NoopAnimationsModule,
        MaterialModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: ListingService,
          useValue: listingService,
        },
        {
          provide: StorageService,
          useValue: {
            getSelectedDomain: (...args) => '',
            getDomainUrl: (...args) => [],
            setSelectedDomain: (...args) => [],
            getDomainList: (...args) => [],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
