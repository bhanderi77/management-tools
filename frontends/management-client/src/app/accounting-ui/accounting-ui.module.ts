import { NgModule } from '@angular/core';
import { AccountComponent } from './account/account.component';
import { JournalEntryComponent } from './journal-entry/journal-entry.component';
import { ReportsComponent } from './reports/reports.component';
import { SettingsComponent } from './settings/settings.component';
import { TreeComponent } from './tree/tree.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { CommonModule } from '@angular/common';
import { JournalEntryService } from './journal-entry/journal-entry.service';
import { ReportsService } from './reports/reports.service';
import { SettingsService } from './settings/settings.service';
import { TreeService } from './tree/tree.service';

@NgModule({
  declarations: [
    AccountComponent,
    JournalEntryComponent,
    ReportsComponent,
    SettingsComponent,
    TreeComponent,
  ],
  imports: [SharedImportsModule, CommonModule],
  exports: [
    AccountComponent,
    JournalEntryComponent,
    ReportsComponent,
    SettingsComponent,
    TreeComponent,
  ],
  providers: [
    JournalEntryService,
    ReportsService,
    SettingsService,
    TreeService,
  ],
})
export class AccountingUIModule {}
