import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SettingsComponent } from './settings.component';
import { SettingsService } from './settings.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { from, of } from 'rxjs';
import { oauthServiceStub } from '../../common/testing-helpers';
import { AppService } from '../../app.service';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: AppService,
          useValue: {},
        },
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => ({}),
            getDomainList: (...args) => [],
            getDomainSettings: (...args) => [],
            setSelectedDomain: (...args) => {},
            getSelectedDomainSettings: (...args) => of({}),
            getSelectedDomain: (...args) => [],
          },
        },
        {
          provide: SettingsService,
          useValue: {
            getSettings: (...args) => from([]),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
