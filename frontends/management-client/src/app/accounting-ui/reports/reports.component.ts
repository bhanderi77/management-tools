import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDatepickerInputEvent, MatSnackBar } from '@angular/material';
import { ReportsService } from './reports.service';
import { DomSanitizer } from '@angular/platform-browser';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { ACCOUNTING_SERVER, CLOSE } from '../../constants/storage';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
})
export class ReportsComponent implements OnInit {
  fromDate: Date;
  toDate: Date;
  toDateResolved: string;
  report: any[];
  domainList: string[];
  fromDateResolved: string;
  csvReport: any;
  blob: any;
  domain: string;
  selectedDomain: any;
  reportForm = new FormGroup({
    fromDate: new FormControl(this.fromDate),
    toDate: new FormControl(this.toDate),
    domain: new FormControl(this.domain),
  });
  constructor(
    private reportingService: ReportsService,
    private sanitizer: DomSanitizer,
    private storageService: StorageService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.selectedDomain = this.storageService.getSelectedDomain();
    this.domainList = this.storageService.getDomainList(ACCOUNTING_SERVER);
  }

  updateFromDate(type: string, event: MatDatepickerInputEvent<Date>) {
    this.fromDate = event.value;
    this.fromDateResolved =
      this.fromDate.getFullYear().toString() +
      '-' +
      (this.fromDate.getMonth() + 1).toString() +
      '-' +
      this.fromDate.getDate().toString();
  }

  updateToDate(type: string, event: MatDatepickerInputEvent<Date>) {
    this.toDate = event.value;
    this.toDateResolved =
      this.toDate.getFullYear().toString() +
      '-' +
      (this.toDate.getMonth() + 1).toString() +
      '-' +
      this.toDate.getDate().toString();
  }

  getProfitReports() {
    if (this.fromDateResolved && this.toDateResolved) {
      this.reportingService
        .getProfitReport(this.fromDateResolved, this.toDateResolved)
        .subscribe({
          next: (res: any) => {
            this.blob = new Blob([res.toString()], {
              type: 'text/csv;charset=utf-8;',
            });
            this.csvReport = this.sanitizer.bypassSecurityTrustResourceUrl(
              window.URL.createObjectURL(this.blob),
            );
            this.SuccessSnackBar();
          },
          error: error => {},
        });
    }
  }

  getBalanceFromReport() {
    if (this.fromDateResolved && this.toDateResolved) {
      this.reportingService
        .getBalanceReports(this.fromDateResolved, this.toDateResolved)
        .subscribe({
          next: (res: any) => {
            this.blob = new Blob([res.toString()], {
              type: 'text/csv;charset=utf-8;',
            });
            this.csvReport = this.sanitizer.bypassSecurityTrustResourceUrl(
              window.URL.createObjectURL(this.blob),
            );
            this.SuccessSnackBar();
          },
          error: error => {},
        });
    } else {
      this.snackBar.open('Select Date please', CLOSE, { duration: 2500 });
    }
  }

  getExpenditureReport() {
    if (this.fromDateResolved && this.toDateResolved) {
      this.reportingService
        .getExpenditureReport(this.fromDateResolved, this.toDateResolved)
        .subscribe({
          next: (res: any) => {
            this.blob = new Blob([res.toString()], {
              type: 'text/csv;charset=utf-8;',
            });
            this.csvReport = this.sanitizer.bypassSecurityTrustResourceUrl(
              window.URL.createObjectURL(this.blob),
            );
            this.SuccessSnackBar();
          },
          error: error => {},
        });
    } else {
      this.snackBar.open('Select Date please', CLOSE, { duration: 2500 });
    }
  }

  getBalanceReport() {
    if (this.fromDateResolved && this.toDateResolved) {
      this.reportingService
        .getTimeReport(this.fromDateResolved, this.toDateResolved)
        .subscribe({
          next: (res: any) => {
            this.blob = new Blob([res.toString()], {
              type: 'text/csv;charset=utf-8;',
            });
            this.csvReport = this.sanitizer.bypassSecurityTrustResourceUrl(
              window.URL.createObjectURL(this.blob),
            );
            this.SuccessSnackBar();
          },
          error: error => {},
        });
    } else {
      this.snackBar.open('Select Date please', CLOSE, { duration: 2500 });
    }
  }

  async getDomainData() {
    const url = this.reportForm.controls.domain.value;
    if (!url) {
      this.snackBar.open('please Select a Domain', CLOSE, { duration: 3500 });
      return;
    }
    this.selectedDomain = url;
    this.storageService.setSelectedDomain(url.url);
    this.snackBar.open('Domain Set To ' + url.type, CLOSE, { duration: 2500 });
  }

  SuccessSnackBar() {
    this.snackBar.open('File Ready Please Download', CLOSE, { duration: 2500 });
  }
}
