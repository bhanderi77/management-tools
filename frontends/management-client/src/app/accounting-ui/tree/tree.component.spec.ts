import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TreeComponent } from './tree.component';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { oauthServiceStub } from '../../common/testing-helpers';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { TreeService } from './tree.service';
import { from } from 'rxjs';

describe('TreeComponent', () => {
  let component: TreeComponent;
  let fixture: ComponentFixture<TreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [TreeComponent],
      providers: [
        {
          provide: Router,
          useValue: {},
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => ({}),
            getSelectedDomain: (...args) => [],
            getDomainList: (...args) => [],
            setSelectedDomain: (...args) => {},
          },
        },
        {
          provide: TreeService,
          useValue: {
            uploadCSV: (...args) => {},
            getParents: (...args) => from([]),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
