export class AccountNode {
  constructor(
    public item: string,
    public accountNumber: string,
    public accountName: string,
    public accountType: string,
    public parentId: string,
    public isGroup: boolean,
    public uuid: string,
    public isRoot: string,
    public level = 1,
    public expandable = false,
    public children: AccountNode[] = [],
  ) {}
}
