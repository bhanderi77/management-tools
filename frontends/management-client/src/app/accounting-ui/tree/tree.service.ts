import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { map } from 'rxjs/operators';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { SELECTED_DOMAIN } from '../../constants/storage';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TreeService {
  authorizationHeader: HttpHeaders;

  constructor(
    private http: HttpClient,
    private oauthService: OAuthService,
    private storageService: StorageService,
  ) {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  uploadCSV(file) {
    const accountingServerUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    const uploadData = new FormData();
    uploadData.append('file', file, file.name);
    const url = accountingServerUrl + '/bulk-csv/upload';
    return this.http.post(url, uploadData, {
      headers: this.authorizationHeader,
    });
  }

  getParents() {
    const accountingServerUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    return this.http
      .get(`${accountingServerUrl}/tree/v1/getRoots`, {
        headers: this.authorizationHeader,
      })
      .pipe(
        map(res => {
          return res;
        }),
      );
  }

  getChildrens(accountNumber) {
    const accountingServerUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    return this.http.get<any>(
      `${accountingServerUrl}/tree/v1/${accountNumber}`,
      {
        headers: this.authorizationHeader,
      },
    );
  }
}
