import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { isDevMode } from '@angular/core';
import {
  OAuthService,
  JwksValidationHandler,
  AuthConfig,
} from 'angular-oauth2-oidc';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  OPENID_ROLES,
  CLOSE,
} from './constants/storage';
import { MatSnackBar } from '@angular/material';
import { StorageService } from './common/services/storage-service/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(
    private appService: AppService,
    private storage: StorageService,
    private snackbar: MatSnackBar,
    private oauthService: OAuthService,
  ) {
    this.setupOIDC();
  }

  ngOnInit() {}

  setupOIDC(): void {
    this.appService.getMessage().subscribe(response => {
      if (response.message) {
        this.snackbar.open(response.message, CLOSE, { duration: 2500 });
        return;
      } // { message: PLEASE_RUN_SETUP }
      this.storage.setInfoLocalStorage(response);
      const authConfig: AuthConfig = {
        clientId: localStorage.getItem(CLIENT_ID),
        redirectUri: localStorage.getItem(REDIRECT_URI),
        silentRefreshRedirectUri: localStorage.getItem(
          SILENT_REFRESH_REDIRECT_URI,
        ),
        loginUrl: localStorage.getItem(LOGIN_URL),
        scope: OPENID_ROLES,
        issuer: localStorage.getItem(ISSUER_URL),
        requireHttps: false,
      };
      if (isDevMode()) authConfig.requireHttps = false;
      this.oauthService.configure(authConfig);
      this.oauthService.tokenValidationHandler = new JwksValidationHandler();
      this.oauthService.setupAutomaticSilentRefresh();
      this.oauthService.loadDiscoveryDocumentAndLogin();
    });
  }
}
