import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { SELECTED_INVOICE_SERVER } from '../../constants/storage';

@Injectable({
  providedIn: 'root',
})
export class PurchaseInvoiceService {
  authorizationHeader: HttpHeaders;
  appUrl: string;
  model: string = '/purchase_invoice';
  api = '/api';
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly oauthService: OAuthService,
  ) {
    this.appUrl = this.storage.getInfo(SELECTED_INVOICE_SERVER) + this.api;
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  GetPurchaseInvoice(uuid) {
    return this.http.get(this.appUrl + this.model + '/v1/get/' + uuid, {
      headers: this.authorizationHeader,
    });
  }

  updatePurchaseInvoice(
    req_invoiceName,
    req_customerId,
    req_customerName,
    req_date,
    req_invoiceNumber,
    req_termsAndConditions,
    req_invoiceItems,
    req_tax,
  ) {
    const newPurchaseInvoice = {
      invoiceName: req_invoiceName,
      customerId: req_customerId,
      customerName: req_customerName,
      date: req_date,
      invoiceNumber: req_invoiceNumber,
      invoiceItems: req_invoiceItems,
      tax: req_tax,
      termsAndConditions: req_termsAndConditions,
    };

    return this.http.post(
      this.appUrl + this.model + '/v1/update',
      newPurchaseInvoice,
      { headers: this.authorizationHeader },
    );
  }

  createNewPurchaseInvoice(
    req_invoiceName,
    req_customerId,
    req_customerName,
    req_date,
    req_invoiceNumber,
    req_termsAndConditions,
    req_invoiceItems,
    req_tax,
  ) {
    const newPurchaseInvoice = {
      invoiceName: req_invoiceName,
      customerId: req_customerId,
      customerName: req_customerName,
      date: req_date,
      invoiceNumber: req_invoiceNumber,
      invoiceItems: req_invoiceItems,
      tax: req_tax,
      termsAndConditions: req_termsAndConditions,
    };

    return this.http.post(
      this.appUrl + this.model + '/v1/create',
      newPurchaseInvoice,
      { headers: this.authorizationHeader },
    );
  }
}
