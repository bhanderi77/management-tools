import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NEW_ID, CLOSE, ELEMENT_DATA } from '../../constants/storage';
import { PurchaseInvoiceService } from './purchase-invoice.service';
import { MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-purchase-invoice',
  templateUrl: './purchase-invoice.component.html',
  styleUrls: ['./purchase-invoice.component.css'],
})
export class PurchaseInvoiceComponent implements OnInit {
  cur: string = 'INR';
  elementData = ELEMENT_DATA;
  uuid: string = undefined;
  purchaseInvoiceForm: FormGroup;
  button: boolean;
  dataSource = new MatTableDataSource(this.elementData);
  form: FormGroup;
  invoiceName: string;
  constructor(
    private readonly fb: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
    private readonly snackBar: MatSnackBar,
  ) {
    this.createForm();
    this.addItem();
    this.uuid = this.activatedRoute.snapshot.params.uuid;
  }

  ngOnInit() {
    this.purchaseInvoiceForm = this.fb.group({
      invoiceName: '',
      customerId: '',
      customerName: '',
      date: '',
      invoiceNumber: '',
      termsAndConditions: '',
    });
    if (this.uuid && this.uuid !== NEW_ID) {
      this.invoiceName = this.form.controls.invoiceName.value;
      this.button = true;
      this.subscribeGetPurchaseInvoice(this.uuid);
    } else {
      this.button = false;
      this.purchaseInvoiceForm.controls.date.setValue(new Date());
      this.purchaseInvoiceForm.controls.date.disable();
    }
  }

  subscribeGetPurchaseInvoice(uuid) {
    this.purchaseInvoiceService.GetPurchaseInvoice(uuid).subscribe({
      next: (response: any) => {
        this.purchaseInvoiceForm.controls.invoiceName.setValue(
          response.invoiceName,
        );
        this.purchaseInvoiceForm.controls.customerId.setValue(
          response.customerId,
        );
        this.purchaseInvoiceForm.controls.customerName.setValue(
          response.customerName,
        );
        this.purchaseInvoiceForm.controls.date.setValue(response.date);
        this.purchaseInvoiceForm.controls.invoiceNumber.setValue(
          response.invoiceNumber,
        );
        this.purchaseInvoiceForm.controls.termsAndConditions.setValue(
          response.termsAndConditions,
        );
        this.purchaseInvoiceForm.controls.invoiceNumber.disable();
        this.invoiceItems.controls.splice(0, 1);
        this.form.controls.taxPercent.setValue(response.tax || 0);
        response.invoiceItems.forEach(element => {
          const invoiceItem = new InvoiceItem();
          const data = element;
          invoiceItem.stt = data.stt;
          invoiceItem.total = data.total;
          invoiceItem.name = data.name;
          invoiceItem.unit = data.unit;
          invoiceItem.qty = data.qty;
          invoiceItem.cost = data.cost;
          this.invoiceItems.push(this.fb.group(invoiceItem));
        });
      },
    });
  }

  updatePurchaseInvoice() {
    this.purchaseInvoiceService
      .updatePurchaseInvoice(
        this.purchaseInvoiceForm.controls.invoiceName.value,
        this.purchaseInvoiceForm.controls.customerId.value,
        this.purchaseInvoiceForm.controls.customerName.value,
        this.purchaseInvoiceForm.controls.date.value,
        this.purchaseInvoiceForm.controls.invoiceNumber.value,
        this.purchaseInvoiceForm.controls.termsAndConditions.value,
        this.invoiceItems.value,
        this.form.controls.taxPercent.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open('Purchase Invoice Updated', CLOSE, {
            duration: 2500,
          });
        },
        error: err => {
          this.snackBar.open('Error in updating Purchase Invoice', CLOSE, {
            duration: 2500,
          });
        },
      });
  }

  createNewPurchaseInvoice() {
    this.purchaseInvoiceService
      .createNewPurchaseInvoice(
        this.purchaseInvoiceForm.controls.invoiceName.value,
        this.purchaseInvoiceForm.controls.customerId.value,
        this.purchaseInvoiceForm.controls.customerName.value,
        this.purchaseInvoiceForm.controls.date.value,
        this.purchaseInvoiceForm.controls.invoiceNumber.value,
        this.purchaseInvoiceForm.controls.termsAndConditions.value,
        this.invoiceItems.value,
        this.form.controls.taxPercent.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open('Purchase Invoice Created', CLOSE, {
            duration: 2500,
          });
        },
      });
  }

  get invoiceItems(): FormArray {
    return this.form.get('invoiceItems') as FormArray;
  }

  toggleLogo() {}
  editLogo() {}

  addItem() {
    this.invoiceItems.push(this.fb.group(new InvoiceItem()));
  }

  cloneItem(item) {
    const invoiceItem = new InvoiceItem();
    const i = this.invoiceItems.controls.indexOf(item);
    const data = this.invoiceItems.value[i];
    invoiceItem.stt = data.stt;
    invoiceItem.total = data.total;
    invoiceItem.name = data.name;
    invoiceItem.unit = data.unit;
    invoiceItem.qty = data.qty;
    invoiceItem.cost = data.cost;
    this.invoiceItems.push(this.fb.group(invoiceItem));
  }

  removeItem(item) {
    const i = this.invoiceItems.controls.indexOf(item);
    if (i !== -1) {
      this.invoiceItems.controls.splice(i, 1);
      const items = this.form.get('invoiceItems') as FormArray;
      const data = { invoiceItems: items };
      this.updateForm(data);
    }
  }

  updateForm(data) {
    const items = data.invoiceItems;
    let sub = 0;
    for (const i of items) {
      if (!i) {
        sub = data.subTotal;
        break;
      }
      i.total = i.qty * i.cost;
      sub += i.total;
    }

    const tax = sub * (this.form.controls.taxPercent.value / 100);
    if (this.form.controls.tax.value !== tax) {
      this.form.controls.tax.setValue(tax);
    }
    if (this.form.controls.subTotal.value !== sub) {
      this.form.controls.subTotal.setValue(sub);
    }
    if (this.form.controls.grantTotal.value !== sub + tax) {
      this.form.controls.grantTotal.setValue(sub + tax);
    }
  }
  createForm() {
    this.form = this.fb.group({
      invoiceName: '',
      invoiceItems: this.fb.array([]),
      subTotal: { value: 0, disabled: true },
      taxPercent: 0,
      tax: 0,

      grantTotal: [{ value: 0, disabled: true }],
    });

    this.form.valueChanges.subscribe(data => this.updateForm(data));
  }
}

export class InvoiceItem {
  stt = '';
  name = '';
  unit = '';
  qty = 0;
  cost = 0;
  total = 0;
}
