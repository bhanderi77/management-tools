import { TestBed } from '@angular/core/testing';
import { PurchaseInvoiceService } from './purchase-invoice.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { oauthServiceStub } from '../../common/testing-helpers';

describe('PurchaseInvoiceService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getServiceURL: (...args) => {},
            getInfo: (...args) => {},
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: PurchaseInvoiceService = TestBed.get(PurchaseInvoiceService);
    expect(service).toBeTruthy();
  });
});
