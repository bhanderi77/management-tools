import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceSettingsComponent } from './invoice-settings.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { InvoiceSettingsService } from './invoice-settings.service';
import { of } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from 'src/app/common/testing-helpers';

describe('InvoiceSettingsComponent', () => {
  let component: InvoiceSettingsComponent;
  let fixture: ComponentFixture<InvoiceSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceSettingsComponent],
      imports: [
        HttpClientTestingModule,
        BrowserDynamicTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: StorageService,
          useValue: {
            getSelectedDomain: (...args) => [],
            getDomainList: (...args) => [],
            setSelectedDomain: (...args) => {},
            domainJson: (...args) => [],
          },
        },
        {
          provide: InvoiceSettingsService,
          useValue: {
            getSelectedDomainSettings: (...args) => of({}),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
