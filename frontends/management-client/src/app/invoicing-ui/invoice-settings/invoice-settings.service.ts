import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { SELECTED_INVOICE_SERVER } from '../../constants/storage';

@Injectable({
  providedIn: 'root',
})
export class InvoiceSettingsService {
  authorizationHeader: HttpHeaders;
  constructor(
    private readonly http: HttpClient,
    private readonly oauthService: OAuthService,
    private readonly storageService: StorageService,
  ) {}

  setupHeaders() {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  getSelectedDomainSettings() {
    this.setupHeaders();
    const url = this.storageService.getInfo(SELECTED_INVOICE_SERVER);
    return this.http.get(url + '/api/info', {
      headers: this.authorizationHeader,
    });
  }

  setAccountingServer(selectedServer) {
    this.setupHeaders();
    const accountingURL = { accountingServer: selectedServer.url };
    const url = this.storageService.getInfo(SELECTED_INVOICE_SERVER);
    return this.http.post(
      url + '/api/settings/v1/setup_accounting_server',
      accountingURL,
      { headers: this.authorizationHeader },
    );
  }
}
