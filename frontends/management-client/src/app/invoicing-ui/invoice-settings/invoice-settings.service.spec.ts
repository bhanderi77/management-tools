import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { InvoiceSettingsService } from './invoice-settings.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../common/testing-helpers';
import { StorageService } from '../../common/services/storage-service/storage.service';

describe('InvoiceSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => '',
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: InvoiceSettingsService = TestBed.get(InvoiceSettingsService);
    expect(service).toBeTruthy();
  });
});
