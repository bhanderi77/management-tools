import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { NEW_ID, CLOSE, ELEMENT_DATA, ERROR } from '../../constants/storage';
import { SalesInvoiceService } from './sales-invoice.service';
import { map, debounceTime } from 'rxjs/operators';
import { ListingService } from '../../common/services/listing-service/listing.service';
// import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-sales-invoice',
  templateUrl: './sales-invoice.component.html',
  styleUrls: ['./sales-invoice.component.css'],
})
export class SalesInvoiceComponent implements OnInit {
  uuid: string;
  salesInvoiceForm: FormGroup;
  button: boolean;
  search: string;
  searchInput: string;
  accounts;
  elementData = ELEMENT_DATA;
  form: FormGroup;
  cur: string = 'INR';
  invoiceName: string;
  myControl;
  constructor(
    private readonly fb: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
    private readonly accountListingService: ListingService,
  ) {
    this.createForm();
    this.addItem();
    this.uuid = this.activatedRoute.snapshot.params.uuid;
  }

  ngOnInit() {
    this.salesInvoiceForm = this.fb.group({
      invoiceName: '',
      customerId: '',
      customerName: '',
      account: {},
      date: '',
      invoiceNumber: '',
      termsAndConditions: '',
      searchInput: '',
    });
    if (this.uuid && this.uuid !== NEW_ID) {
      this.myControl = this.form.controls.myControl.value;
      this.invoiceName = this.form.controls.invoiceName.value;
      this.searchInput = this.form.controls.searchInput.value;
      this.button = true;
      this.subscribeGetSalesInvoice(this.uuid);
    } else {
      this.button = false;
      this.salesInvoiceForm.controls.date.setValue(new Date());
      this.salesInvoiceForm.controls.date.disable();
    }
  }

  subscribeGetSalesInvoice(uuid) {
    this.salesInvoiceService.GetsalesInvoice(uuid).subscribe({
      next: (response: any) => {
        this.salesInvoiceForm.controls.invoiceName.setValue(
          response.invoiceName,
        );
        this.salesInvoiceForm.controls.customerId.setValue(response.customerId);
        this.salesInvoiceForm.controls.customerName.setValue(
          response.customerName,
        );
        this.salesInvoiceForm.controls.date.setValue(response.date);
        this.salesInvoiceForm.controls.invoiceNumber.setValue(
          response.invoiceNumber,
        );
        this.salesInvoiceForm.controls.termsAndConditions.setValue(
          response.termsAndConditions,
        );
        this.salesInvoiceForm.controls.invoiceNumber.disable();
        this.invoiceItems.controls.splice(0, 1);
        response.invoiceItems.forEach(element => {
          const invoiceItem = new InvoiceItem();
          const data = element;
          invoiceItem.stt = data.stt;
          invoiceItem.total = data.total;
          invoiceItem.description = data.description;
          invoiceItem.accountName = data.accountName;
          invoiceItem.accountNumber = data.accountNumber;
          invoiceItem.unit = data.unit;
          invoiceItem.qty = data.qty;
          invoiceItem.cost = data.cost;
          this.invoiceItems.push(this.fb.group(invoiceItem));
        });
        this.form.controls.taxPercent.setValue(response.tax || 0);
      },
    });
  }

  createNewSalesInvoice() {
    this.salesInvoiceService
      .createNewsalesInvoice(
        this.salesInvoiceForm.controls.invoiceName.value,
        this.salesInvoiceForm.controls.customerId.value,
        this.salesInvoiceForm.controls.customerName.value,
        this.salesInvoiceForm.controls.date.value,
        this.salesInvoiceForm.controls.invoiceNumber.value,
        this.salesInvoiceForm.controls.termsAndConditions.value,
        this.invoiceItems.value,
        this.form.controls.taxPercent.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open('Sales Invoice Created', CLOSE, {
            duration: 2500,
          });
          this.router.navigateByUrl('/listing/sales_invoice');
        },
      });
  }

  get invoiceItems(): FormArray {
    return this.form.get('invoiceItems') as FormArray;
  }

  toggleLogo() {}
  editLogo() {}

  addItem() {
    this.invoiceItems.push(this.fb.group(new InvoiceItem()));
  }

  updateSalesInvoice() {
    this.salesInvoiceService
      .updateSalesInvoice(
        this.salesInvoiceForm.controls.invoiceName.value,
        this.salesInvoiceForm.controls.customerId.value,
        this.salesInvoiceForm.controls.customerName.value,
        this.salesInvoiceForm.controls.date.value,
        this.salesInvoiceForm.controls.invoiceNumber.value,
        this.salesInvoiceForm.controls.termsAndConditions.value,
        this.invoiceItems.value,
        this.form.controls.taxPercent.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open('Purchase Invoice Updated', CLOSE, {
            duration: 2500,
          });
        },
        error: err => {
          this.snackBar.open('Error in updating Purchase Invoice', CLOSE, {
            duration: 2500,
          });
        },
      });
  }

  cloneItem(item) {
    const invoiceItem = new InvoiceItem();
    const i = this.invoiceItems.controls.indexOf(item);
    const data = this.invoiceItems.value[i];
    invoiceItem.stt = data.stt;
    invoiceItem.total = data.total;
    invoiceItem.description = data.description;
    invoiceItem.accountName = data.accountName;
    invoiceItem.accountNumber = data.accountNumber;
    invoiceItem.unit = data.unit;
    invoiceItem.qty = data.qty;
    invoiceItem.cost = data.cost;
    this.invoiceItems.push(this.fb.group(invoiceItem));
  }

  removeItem(item) {
    const i = this.invoiceItems.controls.indexOf(item);
    if (i !== -1) {
      this.invoiceItems.controls.splice(i, 1);
      const items = this.form.get('invoiceItems') as FormArray;
      const data = { invoiceItems: items };
      this.updateForm(data);
    }
  }

  updateForm(data) {
    const items = data.invoiceItems;
    let sub = 0;
    for (const i of items) {
      if (!i) {
        sub = data.subTotal;
        break;
      }
      i.total = i.qty * i.cost;
      sub += i.total;
    }
    const tax = sub * (this.form.controls.taxPercent.value / 100);
    if (this.form.controls.tax.value !== tax) {
      this.form.controls.tax.setValue(tax);
    }
    if (this.form.controls.subTotal.value !== sub) {
      this.form.controls.subTotal.setValue(sub);
    }
    if (this.form.controls.grantTotal.value !== sub + tax) {
      this.form.controls.grantTotal.setValue(sub + tax);
    }
  }

  createForm() {
    this.form = this.fb.group({
      invoiceName: '',
      invoiceItems: this.fb.array([]),
      subTotal: { value: 0, disabled: true },
      taxPercent: 0,
      tax: 0,
      grantTotal: { value: 0, disabled: true },
    });

    this.form.valueChanges.subscribe(data => this.updateForm(data));
  }
  searchKeyUp() {
    this.search = this.salesInvoiceForm.controls.searchInput.value;
    this.subscribeListAccounts();
  }

  subscribeListAccounts() {
    this.salesInvoiceService.getInvoiceServer().subscribe({
      next: (response: any) => {
        if (response.accountingServer) {
          this.accounts = this.accountListingService
            .findModels(
              'account',
              this.search,
              '',
              0,
              10,
              `"isGroup" = false`,
              response.accountingServer,
            )
            .pipe(
              map((resp: any) => {
                return resp.docs;
              }),
              debounceTime(1000),
            );
        }
      },
      error: err => {
        this.snackBar.open(ERROR, CLOSE, { duration: 2500 });
      },
    });
  }
}

export class InvoiceItem {
  stt: string = '';
  accountNumber: string = '';
  accountName: string = '';
  description: string = '';
  unit: string = 'Nos';
  qty: number = 0;
  cost: number = 0;
  total: number = 0;
}
