import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { SELECTED_INVOICE_SERVER } from '../../constants/storage';

@Injectable({
  providedIn: 'root',
})
export class SalesInvoiceService {
  authorizationHeader: HttpHeaders;
  appUrl: string;
  model: string = '/sales_invoice';
  api = '/api';
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly oauthService: OAuthService,
  ) {
    this.appUrl = this.storage.getInfo(SELECTED_INVOICE_SERVER) + this.api;
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  GetsalesInvoice(uuid) {
    return this.http.get(this.appUrl + this.model + '/v1/get/' + uuid, {
      headers: this.authorizationHeader,
    });
  }
  updateSalesInvoice(
    req_invoiceName,
    req_customerId,
    req_customerName,
    req_date,
    req_invoiceNumber,
    req_termsAndConditions,
    req_invoiceItems,
    req_tax,
  ) {
    const newSalesInvoice = {
      invoiceName: req_invoiceName,
      customerId: req_customerId,
      customerName: req_customerName,
      date: req_date,
      invoiceNumber: req_invoiceNumber,
      termsAndConditions: req_termsAndConditions,
      invoiceItems: req_invoiceItems,
      tax: req_tax,
    };
    return this.http.post(
      this.appUrl + this.model + '/v1/update',
      newSalesInvoice,
      { headers: this.authorizationHeader },
    );
  }

  getInvoiceServer() {
    return this.http.get(
      localStorage.getItem(SELECTED_INVOICE_SERVER) + '/api/info',
    );
  }

  createNewsalesInvoice(
    req_invoiceName,
    req_customerId,
    req_customerName,
    req_date,
    req_invoiceNumber,
    req_termsAndConditions,
    req_invoiceItems,
    req_tax,
  ) {
    const newSalesInvoice = {
      invoiceName: req_invoiceName,
      customerId: req_customerId,
      customerName: req_customerName,
      date: req_date,
      invoiceNumber: req_invoiceNumber,
      termsAndConditions: req_termsAndConditions,
      invoiceItems: req_invoiceItems,
      tax: req_tax,
    };
    return this.http.post(
      this.appUrl + this.model + '/v1/create',
      newSalesInvoice,
      { headers: this.authorizationHeader },
    );
  }
}
