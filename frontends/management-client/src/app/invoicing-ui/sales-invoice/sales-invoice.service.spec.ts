import { TestBed } from '@angular/core/testing';
import { SalesInvoiceService } from './sales-invoice.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { oauthServiceStub } from '../../common/testing-helpers';

describe('SalesInvoiceService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => {},
            getServiceURL: (...args) => {},
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: SalesInvoiceService = TestBed.get(SalesInvoiceService);
    expect(service).toBeTruthy();
  });
});
