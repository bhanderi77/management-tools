import { SalesInvoiceComponent } from './sales-invoice/sales-invoice.component';
import { PurchaseInvoiceComponent } from './purchase-invoice/purchase-invoice.component';
import { NgModule } from '@angular/core';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { CommonModule } from '@angular/common';
import { PurchaseInvoiceService } from './purchase-invoice/purchase-invoice.service';
import { SalesInvoiceService } from './sales-invoice/sales-invoice.service';
import { InvoiceSettingsComponent } from './invoice-settings/invoice-settings.component';
import { InvoiceSettingsService } from './invoice-settings/invoice-settings.service';

@NgModule({
  declarations: [
    PurchaseInvoiceComponent,
    SalesInvoiceComponent,
    InvoiceSettingsComponent,
  ],
  imports: [SharedImportsModule, CommonModule],
  exports: [PurchaseInvoiceComponent, SalesInvoiceComponent],
  providers: [
    PurchaseInvoiceService,
    InvoiceSettingsService,
    SalesInvoiceService,
  ],
})
export class InvoicingUIModule {}
