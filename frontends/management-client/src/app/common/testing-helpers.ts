import { OAuthService, LoginOptions } from 'angular-oauth2-oidc';
import { from } from 'rxjs';

export const oauthServiceStub: Partial<OAuthService> = {
  events: from([]),
  hasValidAccessToken: () => false,
  getIdentityClaims() {
    return { roles: [] };
  },
  configure(...args) {},
  setupAutomaticSilentRefresh(...args) {},
  loadDiscoveryDocumentAndTryLogin(options?: LoginOptions): Promise<boolean> {
    return Promise.resolve(true);
  },
  loadDiscoveryDocumentAndLogin(options?: LoginOptions): Promise<boolean> {
    return Promise.resolve(true);
  },
  getAccessToken: () => 'mock_token',
};
