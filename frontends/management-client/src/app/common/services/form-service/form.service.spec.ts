import { TestBed } from '@angular/core/testing';
import { FormService } from './form.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../storage-service/storage.service';
import { oauthServiceStub } from '../../testing-helpers';

describe('FormService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        FormService,
        {
          provide: StorageService,
          useValue: {
            getServiceURL: (...args) => {},
            getInfo: (...args) => {},
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: FormService = TestBed.get(FormService);
    expect(service).toBeTruthy();
  });
});
