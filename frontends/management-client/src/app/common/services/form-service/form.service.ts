import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { SELECTED_DOMAIN } from '../../../constants/storage';
import { StorageService } from '../storage-service/storage.service';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  authorizationHeader: HttpHeaders;
  accountingServerUrl = this.storageService.getInfo(SELECTED_DOMAIN);
  constructor(
    private readonly storageService: StorageService,
    private http: HttpClient,
    private readonly oauthService: OAuthService,
  ) {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  getAccount(model, uuid) {
    const issuer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${issuer}/${model}/v1/get/${uuid}`;
    return this.http.get(url, { headers: this.authorizationHeader });
  }
  deleteAccount(model, uuid) {
    const issuer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${issuer}/${model}/v1/delete/${uuid}`;
    return this.http.delete(url, { headers: this.authorizationHeader });
  }

  createAccount(
    model: string,
    accountNumber: string,
    accountName: string,
    accountType: string,
    isGroup: boolean,
    parent: string,
  ) {
    const issuer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${issuer}/${model}/v1/create`;

    const accountData = {
      accountNumber,
      accountName,
      accountType,
      isGroup,
      parent,
    };
    return this.http.post(url, accountData, {
      headers: this.authorizationHeader,
    });
  }

  uploadCSV(file) {
    const uploadData = new FormData();
    uploadData.append('file', file, file.name);
    const url = this.accountingServerUrl + '/bulk-csv/upload';
    return this.http.post(url, uploadData, {
      headers: this.authorizationHeader,
    });
  }

  updateAccount(uuid, model, accountName, accountNumber) {
    const issuer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${issuer}/${model}/v1/update`;

    const accountData = {
      uuid,
      accountName,
      accountNumber,
    };
    return this.http.put(url, accountData, {
      headers: this.authorizationHeader,
    });
  }

  getJe(model, journalEntryTransactionId) {
    const issuer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${issuer}/${model}/v1/je/create`;

    const payload = {
      journalEntryTransactionId,
    };

    return this.http.post(url, payload, { headers: this.authorizationHeader });
  }
}
