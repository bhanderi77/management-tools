import { Injectable } from '@angular/core';
import {
  SELECTED_DOMAIN,
  SELECTED_INVOICE_SERVER,
} from '../../../constants/storage';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {
  HandleError,
  HttpErrorHandler,
} from '../../../http-error-handler.service';
import { map } from 'rxjs/operators';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../storage-service/storage.service';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  handleError: HandleError;
  authorizationHeader: HttpHeaders;

  constructor(
    private storageService: StorageService,
    httpErrorHandler: HttpErrorHandler,
    private http: HttpClient,
    private oauthService: OAuthService,
  ) {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });

    this.handleError = httpErrorHandler.createHandleError('ListingService');
  }

  findModels(
    model: string,
    filters = '',
    sortOrder = 'ASC',
    pageNumber = 0,
    pageSize = 10,
    query = 'id > 0',
    selected_url?,
  ) {
    let appUrl = selected_url || this.storageService.getInfo(SELECTED_DOMAIN);
    let url: string;
    if (model === 'sales_invoice' || model === 'purchase_invoice') {
      appUrl = this.storageService.getInfo(SELECTED_INVOICE_SERVER);
      url = `${appUrl}/api/${model}/v1/list`;
    } else {
      if (model === 'journalentry') {
        url = `${appUrl}/${model}/v1/list/all`;
      }
      if (model === 'account') {
        url = `${appUrl}/${model}/v1/list`;
      }
    }

    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filters)
      .set('sort', sortOrder)
      .set('filters', query);
    return this.http
      .get(url, { params, headers: this.authorizationHeader })
      .pipe(
        map(data => {
          return data;
        }),
      );
  }
}
