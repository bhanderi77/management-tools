import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { switchMap, map, mergeMap, catchError } from 'rxjs/operators';
import {
  ACCOUNTING_SERVER,
  ACCOUNT_SETTINGS,
  CLOSE,
} from './constants/storage';
import { MatSnackBar } from '@angular/material';
import { SERVICE_DOWN } from './constants/messages';
import { StorageService } from './common/services/storage-service/storage.service';

@Injectable()
export class AppService {
  messageUrl = '/info'; // URL to web api

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private readonly snackBar: MatSnackBar,
  ) {}

  /** GET message from the server */
  getMessage(): Observable<any> {
    return this.http.get<any>(this.messageUrl).pipe(
      switchMap(appInfo => {
        if (appInfo.message) {
          return of(appInfo);
        }
        return this.http.get<any>(appInfo.authServerURL + '/info').pipe(
          map(authInfo => {
            appInfo.services = authInfo.services;
            return appInfo;
          }),
        );
      }),
    );
  }

  getAccountingInfo() {
    const service = this.storage.getDomainList(ACCOUNTING_SERVER);
    const accountSettings = [];
    return from(service).pipe(
      mergeMap(data => {
        return this.http
          .get<any>(data.url + '/info')
          .pipe(
            map(account => {
              accountSettings.push({
                type: data.type,
                accountSetup: account.accountSetup,
                periodClosing: account.periodClosing,
              });
            }),
            catchError(error => {
              this.snackBar.open(SERVICE_DOWN, CLOSE, { duration: 2500 });
              return error;
            }),
          )
          .pipe(
            map((serverSettings: any) => {
              localStorage.setItem(
                ACCOUNT_SETTINGS,
                JSON.stringify(accountSettings),
              );
            }),
          );
      }),
    );
  }
}
