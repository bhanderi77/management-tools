export interface CreateJeResponse {
  entryType: string;
  amount: Float32Array;
  account: string;
  journalEntryTransactionId: number;
}
