cd packages/management-console

echo "Testing Management-Console Server"
npm run format && npm run format:check && npm run lint --fix && npm run test:e2e && npm run test  

echo " Testing Accounting Server...."
cd ../accounting-server
npm run format && npm run format:check && npm run lint && npm run test:e2e && npm run test  

echo "Testing Invoice Server...."
cd ../invoice-server
npm run format && npm run format:check && npm run lint --fix  && npm run test:e2e && npm run test  

echo "Testing Customer Server...."
cd ../customer-service
npm run format && npm run format:check && npm run lint --fix  && npm run test:e2e && npm run test  

tput setaf 1; echo " SCROLL AND LOOK ABOVE MAKE SURE YOU DONT SEE ANYTHING RED. "
tput setaf 3; echo " Note - Make sure to build-docs before you commit just in case of any import errors."
tput setaf 3; echo " Commit if everything passes"
tput setaf 2; echo " Good Job.!!"

