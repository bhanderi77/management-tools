# run chmod -x FILENAME.sh

echo " Testing Management-Client...."
cd frontends/management-client
npm run format && npm run format:check && npm run lint && npm run test:e2e && npm run test  

echo " Testing Customer-portal...."
cd ../customer-portal
npm run format && npm run format:check && npm run lint && npm run test:e2e && npm run test  

tput setaf 1; echo " SCROLL AND LOOK ABOVE MAKE SURE YOU DONT SEE ANYTHING RED. "
tput setaf 3; echo " Note - Make sure to build-docs before you commit just in case of any import errors."
tput setaf 3; echo " Commit if everything passes"
tput setaf 2; echo " Good Job.!!"
