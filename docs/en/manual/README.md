# Installation

Install Docker and Docker compose

Install Infrastructure using building blocks

# TypeScript API Documentation

* [Accounting Server]({{ book.docUrl }}/api/accounting-server/)
* [Management Console Server]({{ book.docUrl }}/api/management-console-server/)
* [Management Console Client]({{ book.docUrl }}/api/management-console-client/)

# Commands for testing

```
# NestJS unit tests (server)
yarn test

# NestJS e2e/integration (server)
yarn test:e2e

# Angular unit tests (client)
yarn test --watch=false --browsers ChromeHeadless

# Angular e2e (client)
yarn e2e

# Format Code and lint (both server and client)
yarn format && yarn lint --fix
```
