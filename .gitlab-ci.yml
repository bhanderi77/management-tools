image: registry.gitlab.com/castlecraft/docker-craft/node-latest-headless-chrome:latest

services:
  - docker:dind
  - mongo
  - redis
  - postgres

variables:
  POSTGRES_PASSWORD: "secret"
  POSTGRES_DB: "accounting"

stages:
  - tests
  - pack
  - deploy
  - setup
  - docs

test_accounting_server:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - packages/accounting-server/**/*
  variables:
    POSTGRESDB_HOST: "postgres"
    POSTGRESDB_NAME: "accounting"
    POSTGRESDB_USER: "postgres"
    POSTGRESDB_PASSWORD: "secret"
    MONGODB_HOST: "mongo"
    MONGODB_NAME: "accounting-server"
    BULL_QUEUE_REDIS_HOST: "redis"
    BULL_QUEUE_REDIS_PORT: "6379"
  before_script:
    - cd packages/accounting-server
    - npm install
  script:
    # set env variable for test
    - export NODE_ENV=test
    # Create .env file
    - dockerize -template docker/env.tmpl:.env
    # Check Code Quality
    - npm run lint
    - npm run format:check
    # Test backend unit tests
    - npm run test
    # Test backend e2e
    - npm run test:e2e
  tags:
    - docker

test_invoice_server:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - packages/invoice-server/**/*
  before_script:
    - cd packages/invoice-server
    - npm install
  script:
    - npm run lint
    - npm run format:check
    - npm run test:e2e
    - npm run test
  tags:
    - docker

test_management_client:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - frontends/management-client/**/*
  variables:
    NODE_ENV: "test"
  before_script:
    - cd frontends/management-client
    - npm install
  script:
    - npm run format:check
    - npm run lint
    - npm run test:e2e
    - npm run test
  tags:
    - docker

test_customer_portal:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - frontends/customer-portal/**/*
  variables:
    NODE_ENV: "test"
  before_script:
    - cd frontends/customer-portal
    - npm install
  script:
    - npm run format:check
    - npm run lint
    - npm run test:e2e
    - npm run test
  tags:
    - docker

test_customer_service:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - packages/customer-service/**/*
  variables:
    NODE_ENV: "test"
  before_script:
    - cd packages/customer-service
    - npm install
  script:
    - npm run format:check
    - npm run lint
    - npm run test:e2e
    - npm run test
  tags:
    - docker

test_management_console:
  stage: tests
  only:
    refs:
      - merge_requests
      - develop
      - master
    changes:
      - packages/management-console/**/*
  before_script:
    - cd packages/management-console
    - npm install
  script:
    - npm run lint
    - npm run format:check
    # set env variable for test
    - export NODE_ENV=test
    - npm run test
    # Test backend e2e
    - npm run test:e2e
    # Test frontend
  tags:
    - docker

pack_staging_accounting_server:
  image: docker
  stage: pack
  environment: staging
  only:
    refs:
      - staging
    changes:
      - packages/accounting-server/**/*
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Accounting Server
    - docker build -t accounting-server -f packages/accounting-server/Dockerfile .
    - docker tag accounting-server $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/accounting-server:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/accounting-server

pack_staging_management_console:
  image: docker
  stage: pack
  only:
    refs:
      - staging
    changes:
      - packages/management-console/**/*
  environment: staging
  only:
    - staging
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Accounting Server
    - docker build -t management-console -f packages/management-console/Dockerfile .
    - docker tag management-console $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/management-console:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/management-console

pack_staging_customer_service:
  image: docker
  stage: pack
  only:
    refs:
      - staging
    changes:
      - packages/customer-service/**/*
  environment: staging
  only:
    - staging
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Accounting Server
    - docker build -t customer-service -f packages/customer-service/Dockerfile .
    - docker tag customer-service $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/customer-service:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/customer-service

pack_staging_customer_portal:
  image: docker
  stage: pack
  only:
    refs:
      - staging
    changes:
      - frontends/customer-portal/**/*
  environment: staging
  only:
    - staging
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Customer Portal
    - docker build -t customer-portal -f frontends/customer-portal/Dockerfile .
    - docker tag customer-portal $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/customer-portal:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/customer-portal

pack_staging_invoice_server:
  image: docker
  stage: pack
  only:
    refs:
      - staging
    changes:
      - packages/invoice-server/**/*
  environment: staging
  only:
    - staging
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Accounting Server
    - docker build -t invoice-server -f packages/invoice-server/Dockerfile .
    - docker tag invoice-server $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/invoice-server:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/invoice-server

pack_staging_management_client:
  image: docker
  stage: pack
  only:
    refs:
      - staging
    changes:
      - frontends/management-client/**/*
  environment: staging
  only:
    - staging
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  script:
    # Management Client
    - docker build -t management-client -f frontends/management-client/Dockerfile .
    - docker tag management-client $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/management-client:edge
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/management-tools/management-client

deploy_staging:
  image: dtzar/helm-kubectl:latest
  stage: deploy
  only:
    - staging
  variables:
    KUBECONFIG: /etc/deploy/config
  before_script:
    - mkdir -p /etc/deploy
    - echo ${kube_config} | base64 -d > ${KUBECONFIG}
    - kubectl config use-context staging
    - helm init --service-account staging-user --client-only
    - helm repo add stable https://kubernetes-charts.storage.googleapis.com/
    - helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
    - helm repo update
  script:
    - ./scripts/deploy.sh

pages:
  stage: docs
  only:
    - staging
  before_script:
    - npm install -g lerna
    - npm install
    - lerna clean -y
    - lerna bootstrap
  script:
    - npm install gitbook-cli -g
    - npm install typedoc -g
    # - gitbook fetch latest # fetch latest stable version
    # - gitbook install # add any requested plugins in book.json
    - gitbook build docs public # build to public path
    - npm run docs
  artifacts:
    paths:
      - public
    expire_in: 4 weeks
