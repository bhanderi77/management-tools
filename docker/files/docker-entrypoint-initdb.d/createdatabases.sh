#!/usr/bin/env bash

echo 'Creating application user(s) and db(s)'

if [[ -z "$MONGODB_PRIMARY_ROOT_USER" ]]; then
	echo "MONGODB_PRIMARY_ROOT_USER is not set"
	exit 1
fi
if [[ -z "$MONGODB_ROOT_PASSWORD" ]]; then
	echo "MONGODB_ROOT_PASSWORD is not set"
	exit 1
fi

mongo management-console \
	--host localhost \
	--port 27017 \
	-u $MONGODB_PRIMARY_ROOT_USER \
	-p $MONGODB_ROOT_PASSWORD \
	--authenticationDatabase admin \
	--eval "db.createUser({user: 'management-console', pwd: '$MONGODB_ROOT_PASSWORD', roles:[{role:'dbOwner', db: 'management-console'}]});"

mongo accounting-server \
	--host localhost \
	--port 27017 \
	-u $MONGODB_PRIMARY_ROOT_USER \
	-p $MONGODB_ROOT_PASSWORD \
	--authenticationDatabase admin \
	--eval "db.createUser({user: 'accounting-server', pwd: '$MONGODB_ROOT_PASSWORD', roles:[{role:'dbOwner', db: 'accounting-server'}]});"

mongo invoice-server \
	--host localhost \
	--port 27017 \
	-u $MONGODB_PRIMARY_ROOT_USER \
	-p $MONGODB_ROOT_PASSWORD \
	--authenticationDatabase admin \
	--eval "db.createUser({user: 'invoice-server', pwd: '$MONGODB_ROOT_PASSWORD', roles:[{role:'dbOwner', db: 'invoice-server'}]});"

mongo customer-service \
	--host localhost \
	--port 27017 \
	-u $MONGODB_PRIMARY_ROOT_USER \
	-p $MONGODB_ROOT_PASSWORD \
	--authenticationDatabase admin \
	--eval "db.createUser({user: 'customer-service', pwd: '$MONGODB_ROOT_PASSWORD', roles:[{role:'dbOwner', db: 'customer-service'}]});"