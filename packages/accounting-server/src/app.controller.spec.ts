import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: AppService,
          useValue: {
            info: (...args) => {
              message: ' info';
            },
          },
        },
      ],
    }).compile();
  });

  describe('info', () => {
    it('should be defined', () => {
      const controller = app.get<AppController>(AppController);
      expect(controller.info()).toBeDefined();
    });
  });
});
