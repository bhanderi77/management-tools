import { IEvent } from '@nestjs/cqrs';

export class NewAccountAddedEvent implements IEvent {
  constructor(public readonly payload: any[], public readonly req: any[]) {}
}
