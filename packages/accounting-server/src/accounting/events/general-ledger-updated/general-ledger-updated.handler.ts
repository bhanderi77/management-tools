import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GeneralLedgerUpdatedEvent } from './general-ledger-updated.event';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';
import { AccountService } from '../../entities/account/account.service';
import { from } from 'rxjs';
import { retry } from 'rxjs/operators';

@EventsHandler(GeneralLedgerUpdatedEvent)
export class GeneralLedgerUpdatedHandler
  implements IEventHandler<GeneralLedgerUpdatedEvent> {
  constructor(
    private readonly socket: BulkCsvGateway,
    private readonly accountService: AccountService,
  ) {}

  async handle(event: GeneralLedgerUpdatedEvent) {
    let account;
    let iterator = 1;
    const payload = event.generalLedgerPayload;
    const transactionDate = event.transactionDate;
    const transactionId = event.journalEntryTransactionId;
    const generalLedgerParams = [];
    let generalLedgerQuery =
      'INSERT INTO general_ledger ( "transactionDate", "amountType","amount", "journalEntryTransactionId","accountId") VALUES';

    for (account of payload.accounts) {
      const accountId = await this.accountService.getRepository().query(
        `
      SELECT id FROM account WhERE "accountNumber" = $1
      `,
        [account.account],
      );
      generalLedgerParams.push(
        transactionDate,
        account.entryType,
        account.amount,
        transactionId,
        accountId[0].id,
      );
      const sQuery =
        ' ( $' +
        iterator +
        ', $' +
        (iterator + 1) +
        ', $' +
        (iterator + 2) +
        ', $' +
        (iterator + 3) +
        ', $' +
        (iterator + 4) +
        '),';
      iterator = iterator + 5;
      generalLedgerQuery += sQuery;
    }

    generalLedgerQuery = generalLedgerQuery.slice(0, -1) + ';';
    from(
      this.accountService
        .getRepository()
        .query(generalLedgerQuery, generalLedgerParams),
    )
      .pipe(retry(2))
      .subscribe({
        next: success => {
          this.socket.handleMessage('General Ledger Successfully Updated');
        },
        error: error => {},
      });
  }
}
