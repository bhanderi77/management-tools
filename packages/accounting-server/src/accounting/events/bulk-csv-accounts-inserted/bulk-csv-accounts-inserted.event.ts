import { IEvent } from '@nestjs/cqrs';

export class BulkCsvAccountsInsertedEvent implements IEvent {
  constructor(
    public readonly csvAccountsPayload: any[],
    public readonly clientHttpRequest: any,
  ) {}
}
