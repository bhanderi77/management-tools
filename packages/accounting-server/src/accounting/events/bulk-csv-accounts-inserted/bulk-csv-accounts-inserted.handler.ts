import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BulkCsvAccountsInsertedEvent } from './bulk-csv-accounts-inserted.event';
import { AccountService } from '../../entities/account/account.service';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';

@EventsHandler(BulkCsvAccountsInsertedEvent)
export class BulkCsvAccountsInsertedHandler
  implements IEventHandler<BulkCsvAccountsInsertedEvent> {
  constructor(
    private readonly socketGateway: BulkCsvGateway,
    private readonly accountService: AccountService,
  ) {}
  async handle(event: BulkCsvAccountsInsertedEvent) {
    const bulkAccountPayload = event.csvAccountsPayload;
    const isRoot = false;
    const createdBy = 'event.clientHttpRequest.req.token';
    let dynamicClosureQuery = `INSERT into "account_closure" ("id_ancestor" , "id_descendant") VALUES`;
    let dynamicAccountQuery = `INSERT INTO "account"
      ("uuid", "accountNumber", "accountName", "accountType","isRoot", "isGroup",
      "createdBy", "modifiedBy", "modified", "parentId") VALUES`;
    const dynamicAccountValues = [];
    const dynamicClosureValues = [];
    const insertedAccounts = [];
    let iterator = 1;

    for (const element of bulkAccountPayload) {
      const parent = await this.accountService.findParent(element.parent);

      insertedAccounts.push(element.accountNumber);
      dynamicAccountValues.push(
        element.uuid,
        element.accountNumber,
        element.accountName,
        element.accountType,
        isRoot,
        element.isGroup,
        createdBy,
        createdBy,
        'NOW',
        parent[0].id,
      );

      const subordinateQuery =
        ' ( $' +
        iterator +
        ', $' +
        (iterator + 1) +
        ', $' +
        (iterator + 2) +
        ', $' +
        (iterator + 3) +
        ', $' +
        (iterator + 4) +
        ', $' +
        (iterator + 5) +
        ', $' +
        (iterator + 6) +
        ', $' +
        (iterator + 7) +
        ', $' +
        (iterator + 8) +
        ', $' +
        (iterator + 9) +
        '),';

      iterator = iterator + 10;
      dynamicAccountQuery += subordinateQuery;
    }
    dynamicAccountQuery = dynamicAccountQuery.slice(0, -1) + ';';
    await this.accountService
      .getRepository()
      .query(dynamicAccountQuery, dynamicAccountValues);

    this.socketGateway.handleMessage('CSV successfully inserted');
    iterator = 1;
    for (const accountNumber of insertedAccounts) {
      const account: {
        id: number;
        parentId: number;
      } = await this.accountService.getAccountId(accountNumber);
      dynamicClosureValues.push(account[0].id, account[0].parentId);

      const subQuery = ' ( $' + iterator + ', $' + (iterator + 1) + '),';
      iterator += 2;
      dynamicClosureQuery += subQuery;
    }

    dynamicClosureQuery = dynamicClosureQuery.slice(0, -1) + ';';
    this.accountService
      .getRepository()
      .query(dynamicClosureQuery, dynamicClosureValues);

    this.socketGateway.handleMessage('CSV relations created');
  }
}
