import { AccountNameModifiedHandler } from './account-name-modified/account-name-modified.handler';
import { NewAccountAddedHandler } from './new-account-added/new-account-added.handler';
import { JournalEntryAccountCreatedHandler } from './journal-entry-account-created/journal-entry-account-created.handler';
import { GeneralLedgerUpdatedHandler } from './general-ledger-updated/general-ledger-updated.handler';
import { BulkCsvAccountsInsertedHandler } from './bulk-csv-accounts-inserted/bulk-csv-accounts-inserted.handler';

export const AccountManagementEventHandlers = [
  AccountNameModifiedHandler,
  NewAccountAddedHandler,
  JournalEntryAccountCreatedHandler,
  GeneralLedgerUpdatedHandler,
  BulkCsvAccountsInsertedHandler,
];
