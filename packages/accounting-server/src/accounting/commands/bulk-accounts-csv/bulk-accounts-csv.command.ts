import { ICommand } from '@nestjs/cqrs';

export class BulkCsvAccountsCommand implements ICommand {
  constructor(
    public readonly csvFileData: any,
    public readonly clientActor: any,
  ) {}
}
