import { ICommandHandler, EventPublisher, CommandHandler } from '@nestjs/cqrs';
import { JournalEntryManagerAggregateService } from '../../aggregates/journal-entry-manager-aggregate/journal-entry-manager-aggregate.service';
import { NewJournalEntryAccountCommand } from './new-journal-entry-account.command';
import { BULK_CSV } from '../../../constants/app-strings';
import { BulkAccountsCsvValidationPolicyService } from '../../policies';
import { BadRequestException } from '@nestjs/common';

@CommandHandler(NewJournalEntryAccountCommand)
export class NewJournalEntryAccountHandler
  implements ICommandHandler<NewJournalEntryAccountCommand> {
  constructor(
    private readonly manager: JournalEntryManagerAggregateService,
    private readonly publisher: EventPublisher,
    private readonly csvValidator: BulkAccountsCsvValidationPolicyService,
  ) {}
  async execute(command: NewJournalEntryAccountCommand) {
    const { payload, req, requestType } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);

    if (requestType === BULK_CSV) {
      this.csvValidator.csvToJson(payload).subscribe({
        next: async response => {
          const data = { accounts: response };
          await this.manager.newJournalEntryAccount(data, req);
          aggregate.commit();
        },
        error: err => {
          return new BadRequestException('Please Check Data in CSV');
        },
      });
    } else {
      await this.manager.newJournalEntryAccount(payload, req);
      aggregate.commit();
    }
  }
}
