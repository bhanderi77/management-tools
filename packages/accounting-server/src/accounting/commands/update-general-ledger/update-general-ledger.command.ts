import { ICommand } from '@nestjs/cqrs';
import { Account } from '../../entities/account/account.entity';

export class UpdateGeneralLedgerCommand implements ICommand {
  constructor(
    public readonly generalLedgerPayload: { accounts: Account[] },
    public readonly transactionDate: Date,
    public readonly journalEntryTransactionId: string,
  ) {}
}
