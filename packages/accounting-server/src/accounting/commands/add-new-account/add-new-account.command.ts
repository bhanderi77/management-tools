import { ICommand } from '@nestjs/cqrs';

export class AddNewAccountCommand implements ICommand {
  constructor(public readonly payload: any[], public readonly req: string) {}
}
