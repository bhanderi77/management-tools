import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JournalEntryAccount } from './journal-entry-account.entity';
import { Repository } from 'typeorm';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

@Injectable()
export class JournalEntryAccountService {
  constructor(
    @InjectRepository(JournalEntryAccount, POSTGRES_CONNECTION_NAME)
    private readonly jeAccountRepository: Repository<JournalEntryAccount>,
  ) {}

  async getOne(id, limit, offset, search, sort, query) {
    let account;
    if (!search) {
      account = await this.jeAccountRepository
        .createQueryBuilder('journal_entry_account')
        .orderBy(`"accountId"`, sort)
        .skip(offset)
        .take(limit)
        .getMany();
      return account;
    } else {
      account = await this.jeAccountRepository
        .createQueryBuilder('journal_entry_account')
        .orderBy(`"accountId"`, sort)
        .where(
          `( "entryType" = '${search}' ) or ("amount" = '${search}') or ("journalEntryTransactionId" = '${search}')`,
        )
        .skip(offset)
        .take(limit)
        .getMany();
      return account;
    }
  }

  getRepository() {
    return this.jeAccountRepository;
  }
  // async checkIsLocked(entry) {
  //   if (
  //     (await this.JeAccountRepository.query(
  //       `SELECT "isLocked"
  //     from journal_entry_account
  //     WHERE "transactionId" = $1`,
  //       [entry],
  //     )) === 'TRUE'
  //   ) {
  //     return { message: 'Locked entry not deleted' };
  //   }
  // }
}
