import { Test, TestingModule } from '@nestjs/testing';
import { GeneralLedgerService } from './general-ledger.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GeneralLedger } from './general-ledger.entity';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

describe('GeneralLedgerService', () => {
  let service: GeneralLedgerService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GeneralLedgerService,
        {
          provide: getRepositoryToken(GeneralLedger, POSTGRES_CONNECTION_NAME),
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<GeneralLedgerService>(GeneralLedgerService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
