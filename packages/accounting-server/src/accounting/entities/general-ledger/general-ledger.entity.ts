import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { JournalEntry } from '../journal-entry/journal-entry.entity';
import { Account } from '../account/account.entity';

export type TransactionType = 'CREDIT' | 'DEBIT';

@Entity()
export class GeneralLedger {
  @PrimaryGeneratedColumn()
  transactionId: number;

  @Column()
  transactionDate: Date;

  @ManyToOne(
    type => JournalEntry,
    journalEntry => journalEntry.generalLedgerJournalEntries,
  )
  @JoinColumn()
  journalEntry: JournalEntry;

  @ManyToOne(type => Account, account => account.generalLedgerAccounts)
  @JoinColumn()
  account: Account;

  @Column({ type: 'enum', enum: ['CREDIT', 'DEBIT'] })
  amountType: TransactionType;

  @Column({ type: 'float8' })
  amount: number;
}
