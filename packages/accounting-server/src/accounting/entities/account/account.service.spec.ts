import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from './account.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Account } from './account.entity';
import { CommandBus } from '@nestjs/cqrs';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

describe('AccountService', () => {
  let service: AccountService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountService,
        {
          provide: getRepositoryToken(Account, POSTGRES_CONNECTION_NAME),
          useValue: {},
        },
        {
          provide: CommandBus,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<AccountService>(AccountService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
