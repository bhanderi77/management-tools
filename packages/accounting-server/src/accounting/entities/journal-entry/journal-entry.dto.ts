import { IsNotEmpty } from 'class-validator';
import { JournalEntryAccountDto } from '../journal-entry-account/journal-entry-account.dto';

export class JournalEntryDTO {
  @IsNotEmpty()
  accounts: JournalEntryAccountDto[];
}
