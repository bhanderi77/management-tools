import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { TransactionType } from '../general-ledger/general-ledger.entity';
import { Account } from '../account/account.entity';

@Entity()
export class GeneralLedgerArchive {
  @PrimaryGeneratedColumn()
  transactionId: number;

  @CreateDateColumn()
  transactionDate: Date;

  @OneToOne(type => Account)
  @JoinColumn()
  account: Account;

  @Column({ type: 'enum', enum: ['CREDIT', 'DEBIT'] })
  amountType: TransactionType;

  @Column({ type: 'float8' })
  amount: number;
}
