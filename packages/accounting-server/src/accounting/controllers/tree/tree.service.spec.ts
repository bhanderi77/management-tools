import { Test, TestingModule } from '@nestjs/testing';
import { TreeService } from './tree.service';
import { AccountService } from '../../entities/account/account.service';

describe('TreeService', () => {
  let service: TreeService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TreeService,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<TreeService>(TreeService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
