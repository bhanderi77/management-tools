import { Test, TestingModule } from '@nestjs/testing';
import { BulkCsvController } from './bulk-csv.controller';
import { BulkCsvService } from './bulk-csv.service';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { BulkCsvGateway } from './event/bulk-csv.gateway';
import { CommandBus } from '@nestjs/cqrs';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { HttpService } from '@nestjs/common';

describe('BulkCsv Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [BulkCsvController],
      providers: [
        {
          provide: BulkCsvService,
          useValue: {},
        },
        {
          provide: BulkCsvGateway,
          useValue: {},
        },
        {
          provide: CommandBus,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: BulkCsvController = module.get<BulkCsvController>(
      BulkCsvController,
    );
    expect(controller).toBeDefined();
  });
});
