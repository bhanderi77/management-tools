import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { UseGuards } from '@nestjs/common';
import { Observable, from } from 'rxjs';
import { CheckGuard } from '../../../../auth/guards/check.guard';

@WebSocketGateway({ transports: ['websocket'] })
export class BulkCsvGateway {
  @WebSocketServer() server;

  @SubscribeMessage('message')
  @UseGuards(CheckGuard)
  handleMessage(message): any {
    // this.nameSpace.emit('event' , this.findAll(message))
    this.server.emit('event', this.findAll(message));
    return from('Hello world!');
  }

  successResponse(message): any {
    this.server.emit('success', this.success(message));
    return from('Hello world!');
  }

  @SubscribeMessage('success')
  @UseGuards(CheckGuard)
  success(message): Observable<WsResponse<string>> {
    return message;
  }

  @SubscribeMessage('event')
  @UseGuards(CheckGuard)
  findAll(message): Observable<WsResponse<string>> {
    return message;
  }

  @SubscribeMessage('identity')
  async identity(client, data: string): Promise<string> {
    return data;
  }
}
