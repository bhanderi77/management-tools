import { Injectable, BadRequestException } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import * as csv from './csv-utils/rxjs-csv';
import { of, from, throwError } from 'rxjs';
import { map, toArray, mergeMap, switchMap, concatAll } from 'rxjs/operators';
import { validate } from 'class-validator';
import { AccountDto } from '../../controllers/account/account-dto';
import { BulkCsvGateway } from './event/bulk-csv.gateway';
import { AccountService } from '../../../accounting/entities/account/account.service';

@Injectable()
export class BulkCsvService {
  fs = require('fs');
  constructor(
    private readonly accountService: AccountService,
    private bulkCSV: BulkCsvGateway,
  ) {}
  accountDTO = new AccountDto();

  bulkAccounts(file, req) {
    return this.validateJSON(file).pipe(
      mergeMap((res: any) => {
        return from(
          this.accountService.validateAccount(
            res.accountType,
            res.parent,
            res.accountNumber,
          ),
        ).pipe(
          mergeMap((valid: any) => {
            if (!valid) {
              this.bulkCSV.handleMessage(
                'error in CSV file at Account Number- ' +
                  res.accountNumber +
                  ' parent- ' +
                  res.parent,
              );
              throw new BadRequestException(
                'Invalid account entry at - Account Number ' +
                  res.accountNumber +
                  ' or Parent Id ' +
                  res.parent,
              );
            } else {
              this.bulkCSV.handleMessage(
                'CSV validated , Inserting Data Entry',
              );
              return of(res);
            }
          }),
        );
      }),
      mergeMap((res: any) => {
        return from(this.accountService.create(res, req, true)).pipe(
          map(data => {
            this.bulkCSV.handleMessage('CSV Successfully inserted');
          }),
        );
      }),
    );
  }

  csvToJson(file) {
    const uuid = uuidv4();
    const path = '/tmp/' + uuid + '.csv',
      buffer = Buffer.from(file.buffer.toString());

    this.fs.open(path, 'w', (err, fd) => {
      if (err) {
        throw new BadRequestException('error opening file: ' + err);
      }
      this.fs.write(fd, buffer, 0, buffer.length, null, error => {
        if (error)
          throw new BadRequestException('error writing file: ' + error);
        this.fs.close(fd, () => {});
      });
    });

    return csv.parse(path, { columns: true }).pipe(
      map(res => {
        return res;
      }),
      toArray(),
    );
  }

  validateJSON(file) {
    return this.csvToJson(file).pipe(
      mergeMap((res: any) => {
        return from(res).pipe(
          switchMap((account: any) => {
            account.uuid = uuidv4();
            account.isGroup = JSON.parse(account.isGroup);
            Object.assign(this.accountDTO, account);
            return from(
              validate(this.accountDTO, {
                forbidUnknownValues: true,
                forbidNonWhitelisted: true,
                whitelist: true,
              }),
            );
          }),
          switchMap(validation => {
            if (validation.length === 0) {
              this.bulkCSV.handleMessage('CSV Successfully validated');
              return of(res);
            } else {
              return throwError(new BadRequestException(validation));
            }
          }),
        );
      }),
      concatAll(),
    );
  }
}
