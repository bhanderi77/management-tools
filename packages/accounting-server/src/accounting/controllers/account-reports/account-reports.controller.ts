import { Controller, Get, Query, Header, UseGuards } from '@nestjs/common';
import { AccountReportsService } from './account-reports.service';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import {
  BALANCE_REPORT_ACCOUNT,
  EXPENDITURE_REPORT_ACCOUNT,
} from '../../../constants/app-strings';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ACCOUNTANT } from '../../../constants/roles';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('account-reports')
export class AccountReportsController {
  constructor(private readonly accountReportService: AccountReportsService) {}

  @Get('v1/getProfitReports')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  report(@Query('fromTime') fromTime, @Query('toTime') toTime) {
    return this.accountReportService.getProfitReports(fromTime, toTime).pipe(
      switchMap(data => {
        return from(this.accountReportService.sendProfitReportFile(data));
      }),
    );
  }

  @Get('v1/getExpenditureReport')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  getExpenditureReports(@Query('fromTime') fromTime, @Query('toTime') toTime) {
    return this.accountReportService
      .getReportByTime(fromTime, toTime, EXPENDITURE_REPORT_ACCOUNT)
      .pipe(
        switchMap(data => {
          return from(this.accountReportService.sendProfitReportFile(data));
        }),
      );
  }

  @Get('v1/getBalanceReports')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  getBalanceReports(@Query('fromTime') fromTime, @Query('toTime') toTime) {
    return this.accountReportService
      .getReportByTime(fromTime, toTime, BALANCE_REPORT_ACCOUNT)
      .pipe(
        switchMap(data => {
          return from(this.accountReportService.sendProfitReportFile(data));
        }),
      );
  }

  @Get('v1/timeBasedReport')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  @Header('content-type', 'text/csv')
  timeBasedReport(@Query('fromTime') fromTime, @Query('toTime') toTime) {
    return this.accountReportService.getReportByTime(fromTime, toTime).pipe(
      switchMap(data => {
        return from(this.accountReportService.sendReportFile(data));
      }),
    );
  }
}
