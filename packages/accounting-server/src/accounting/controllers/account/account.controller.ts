import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Req,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AccountDto } from './account-dto';
import { AccountService } from '../../../accounting/entities/account/account.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ACCOUNTANT } from '../../../constants/roles';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post('v1/create')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async createAccount(@Body() payload: AccountDto, @Req() req) {
    return await this.accountService.addNewAccount(payload, req);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async list(
    @Req() req,
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
    @Query('filters') filters?,
  ) {
    sort = sort ? sort.toUpperCase() : 'ASC';

    return this.accountService.find(limit, offset, search, sort, filters);
  }

  @Put('v1/update')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async updateAccount(@Body() payload, @Req() req) {
    return await this.accountService.update(payload, req);
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async getAccount(@Param('uuid') uuid: AccountDto) {
    return await this.accountService.findOne({ uuid });
  }

  @Get('v1/validate/:account_number')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async validateAccount(@Param('account_number') accountNumber) {
    return await this.accountService.validateAccountForJE(accountNumber);
  }
}
