import { Test, TestingModule } from '@nestjs/testing';
import { AccountSetupService } from './account-setup.service';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { AccountService } from '../../entities/account/account.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';

describe('AccountSetupService', () => {
  let service: AccountSetupService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountSetupService,
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<AccountSetupService>(AccountSetupService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
