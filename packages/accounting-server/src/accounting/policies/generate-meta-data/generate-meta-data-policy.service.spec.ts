import { Test, TestingModule } from '@nestjs/testing';
import { GenerateMetaDataPolicy } from './generate-meta-data.service-policy';
import { AccountService } from '../../entities/account/account.service';

describe('GenerateMetaDataPolicy', () => {
  let service: GenerateMetaDataPolicy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GenerateMetaDataPolicy,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<GenerateMetaDataPolicy>(GenerateMetaDataPolicy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
