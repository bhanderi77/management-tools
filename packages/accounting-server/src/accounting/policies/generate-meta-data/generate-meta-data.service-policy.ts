import { Injectable } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class GenerateMetaDataPolicy {
  async GenerateJournalEntryMetaData() {
    const generatedUuid: string = uuidv4();
    const generatedDate: Date = new Date();

    return await {
      uuid: generatedUuid,
      date: generatedDate,
    };
  }
}
