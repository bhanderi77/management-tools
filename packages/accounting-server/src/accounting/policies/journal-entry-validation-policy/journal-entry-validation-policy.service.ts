import { Injectable, BadRequestException } from '@nestjs/common';
import { AccountService } from '../../entities/account/account.service';
import { switchMap, mergeMap, map } from 'rxjs/operators';
import { from, of, throwError } from 'rxjs';
import { JournalEntryAccountDto } from '../../entities/journal-entry-account/journal-entry-account.dto';
import { validate } from 'class-validator';
// import { switchMap } from 'rxjs/operators';
// import { validate } from 'class-validator';

@Injectable()
export class JournalEntryValidationPolicy {
  constructor(private readonly accountService: AccountService) {}

  validationPromise(payload) {
    return this.validateJournalEntry(payload).toPromise();
  }

  validateJournalEntry(payload) {
    let creditAmount = 0;
    let debitAmount = 0;
    for (const data of payload.accounts) {
      data.entryType = data.entryType.toUpperCase();
      data.amount = parseFloat(data.amount);
      if (data.entryType === 'DEBIT') {
        debitAmount += data.amount;
      } else {
        if (data.entryType === 'CREDIT') {
          creditAmount += data.amount;
        } else {
          return throwError(
            new BadRequestException(
              'Invalid Account Entry Type at -' + data.account,
            ),
          );
        }
      }
    }

    for (const data of payload.accounts) {
      return from(this.validateDataObject(data)).pipe(
        mergeMap((objectValidation: any) => {
          if (objectValidation !== true) {
            return throwError(new BadRequestException('Invalid Data'));
          } else {
            if (creditAmount === debitAmount) {
              return from(payload.accounts).pipe(
                switchMap((singleAccount: JournalEntryAccountDto) => {
                  return from(
                    this.accountService
                      .getRepository()
                      .query(
                        `SELECT uuid FROM account WHERE "accountNumber" = $1 and "isGroup" = false `,
                        [singleAccount.account],
                      ),
                  ).pipe(
                    mergeMap(validAccount => {
                      if (!validAccount) {
                        return throwError(
                          new BadRequestException(
                            'Account Number ' +
                              singleAccount.account +
                              'does not exist or it is a group account',
                          ),
                        );
                      }
                      return of(true);
                    }),
                  );
                }),
              );
            } else {
              return throwError(
                new BadRequestException(
                  'Credit Amount not equal to debit amount',
                ),
              );
            }
          }
        }),
      );
    }
  }

  validateDataObject(data) {
    const journalEntryDto = new JournalEntryAccountDto();
    Object.assign(journalEntryDto, data);
    return from(
      validate(journalEntryDto, {
        forbidUnknownValues: true,
        forbidNonWhitelisted: true,
        whitelist: true,
      }),
    ).pipe(
      map((response: any) => {
        if (response.length === 0) {
          return true;
        } else {
          return throwError(new BadRequestException(response));
        }
      }),
    );
  }
}
