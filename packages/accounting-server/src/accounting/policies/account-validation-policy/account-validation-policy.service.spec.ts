import { Test, TestingModule } from '@nestjs/testing';
import { AccountValidationPolicy } from './account-validation-policy.service';
import { AccountService } from '../../entities/account/account.service';

describe('AccountValidationPolicy', () => {
  let service: AccountValidationPolicy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountValidationPolicy,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AccountValidationPolicy>(AccountValidationPolicy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
