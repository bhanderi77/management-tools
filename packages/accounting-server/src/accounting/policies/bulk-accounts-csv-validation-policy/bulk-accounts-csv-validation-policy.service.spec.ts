import { Test, TestingModule } from '@nestjs/testing';
import { BulkAccountsCsvValidationPolicyService } from './bulk-accounts-csv-validation-policy.service';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';
import { AccountService } from '../../entities/account/account.service';

describe('BulkAccountCsvValidationPolicyService', () => {
  let service: BulkAccountsCsvValidationPolicyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BulkAccountsCsvValidationPolicyService,
        {
          provide: BulkCsvGateway,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<BulkAccountsCsvValidationPolicyService>(
      BulkAccountsCsvValidationPolicyService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
