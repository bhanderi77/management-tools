import { Module, Global, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountStatementService } from './entities/account-statement/account-statement.service';
import { GeneralLedgerArchiveService } from './entities/general-ledger-archive/general-ledger-archive.service';
import { JournalEntryAccountService } from './entities/journal-entry-account/journal-entry-account.service';
import { JournalEntryService } from './entities/journal-entry/journal-entry.service';
import { AccountService } from './entities/account/account.service';
import { GeneralLedgerService } from './entities/general-ledger/general-ledger.service';
import { AccountStatement } from './entities/account-statement/account-statement.entity';
import { GeneralLedger } from './entities/general-ledger/general-ledger.entity';
import { GeneralLedgerArchive } from './entities/general-ledger-archive/general-ledger-archive.entity';
import { JournalEntry } from './entities/journal-entry/journal-entry.entity';
import { JournalEntryAccount } from './entities/journal-entry-account/journal-entry-account.entity';
import { Account } from './entities/account/account.entity';
import { CqrsModule } from '@nestjs/cqrs';
import { AccountManagementCommandHandlers } from './commands';
import { AccountManagementEventHandlers } from './events';
import { BulkCsvGateway } from './controllers/bulk-csv/event/bulk-csv.gateway';
import { AccountAggregates } from './aggregates';
import { AccountingPolicies } from './policies';
import { AccountingSagas } from './sagas';
import { GeneralLedgerManagerAggregateService } from './aggregates/general-ledger-manager-aggregate/general-ledger-manager-aggregate.service';
import { POSTGRES_CONNECTION_NAME } from '../constants/postgres.connection';
import { JournalEntryManagementService } from './controllers/journal-entry/journal-entry-management.service';
import { AccountController } from './controllers/account/account.controller';
import { JournalEntryController } from './controllers/journal-entry/journal-entry.controller';
import { AccountSetupController } from './controllers/account-setup/account-setup.controller';
import { AccountReportsController } from './controllers/account-reports/account-reports.controller';
import { TreeController } from './controllers/tree/tree.controller';
import { BulkCsvController } from './controllers/bulk-csv/bulk-csv.controller';
import { AccountSetupService } from './controllers/account-setup/account-setup.service';
import { AccountReportsService } from './controllers/account-reports/account-reports.service';
import { TreeService } from './controllers/tree/tree.service';
import { BulkCsvService } from './controllers/bulk-csv/bulk-csv.service';

@Global()
@Module({
  imports: [
    CqrsModule,
    HttpModule,
    TypeOrmModule.forFeature(
      [
        Account,
        AccountStatement,
        GeneralLedger,
        GeneralLedgerArchive,
        JournalEntry,
        JournalEntryAccount,
      ],
      POSTGRES_CONNECTION_NAME,
    ),
  ],
  controllers: [
    AccountController,
    JournalEntryController,
    AccountController,
    AccountSetupController,
    AccountReportsController,
    TreeController,
    BulkCsvController,
  ],
  providers: [
    ...AccountAggregates,
    ...AccountManagementCommandHandlers,
    ...AccountManagementEventHandlers,
    ...AccountingPolicies,
    ...AccountingSagas,
    AccountReportsService,
    AccountService,
    AccountSetupService,
    AccountStatementService,
    BulkCsvGateway,
    BulkCsvService,
    GeneralLedgerService,
    GeneralLedgerArchiveService,
    GeneralLedgerManagerAggregateService,
    JournalEntryService,
    JournalEntryAccountService,
    JournalEntryManagementService,
    TreeService,
  ],
  exports: [
    AccountReportsService,
    AccountService,
    AccountSetupService,
    AccountStatementService,
    BulkCsvGateway,
    BulkCsvService,
    GeneralLedgerService,
    GeneralLedgerArchiveService,
    GeneralLedgerManagerAggregateService,
    JournalEntryService,
    JournalEntryAccountService,
    JournalEntryManagementService,
    TreeService,
  ],
})
export class AccountingModule {}
