import { AccountManagerAggregate } from './account-manager-aggregate/account-manager-aggregate.service';
import { JournalEntryManagerAggregateService } from './journal-entry-manager-aggregate/journal-entry-manager-aggregate.service';
import { BulkCsvAccountsManagerAggregateService } from './bulk-accounts-csv-manager-aggregate/bulk-accounts-csv-manager-aggregate.service';

export const AccountAggregates = [
  AccountManagerAggregate,
  JournalEntryManagerAggregateService,
  BulkCsvAccountsManagerAggregateService,
];

export {
  AccountManagerAggregate,
} from './account-manager-aggregate/account-manager-aggregate.service';
export {
  JournalEntryManagerAggregateService,
} from './journal-entry-manager-aggregate/journal-entry-manager-aggregate.service';
export {
  BulkCsvAccountsManagerAggregateService,
} from './bulk-accounts-csv-manager-aggregate/bulk-accounts-csv-manager-aggregate.service';
