import { AggregateRoot } from '@nestjs/cqrs';
import { AccountNameModifiedEvent } from '../../../accounting/events/account-name-modified/account-name-modified.event';
import { Injectable } from '@nestjs/common';
import { AccountValidationPolicy } from '../../../accounting/policies/account-validation-policy/account-validation-policy.service';
import { NewAccountAddedEvent } from '../../../accounting/events/new-account-added/new-account-added.event';

@Injectable()
export class AccountManagerAggregate extends AggregateRoot {
  constructor(
    private readonly accountValidationPolicies: AccountValidationPolicy,
  ) {
    super();
  }

  async updateAccountName(
    accountName: string,
    uuid: string,
    modifiedBy: string,
  ) {
    return this.apply(
      new AccountNameModifiedEvent(accountName, modifiedBy, uuid),
    );
  }

  async addNewAccount(payload, req) {
    await this.accountValidationPolicies.validateParentAccountNumber(
      payload.parent,
    );
    await this.accountValidationPolicies.validateAccountNumber(
      payload.accountNumber,
    );
    this.apply(new NewAccountAddedEvent(payload, req));
  }
}
