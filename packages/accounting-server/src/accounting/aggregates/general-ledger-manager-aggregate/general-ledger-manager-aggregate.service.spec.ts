import { Test, TestingModule } from '@nestjs/testing';
import { GeneralLedgerManagerAggregateService } from './general-ledger-manager-aggregate.service';

describe('GeneralLedgerManagerAggregateService', () => {
  let service: GeneralLedgerManagerAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralLedgerManagerAggregateService],
    }).compile();

    service = module.get<GeneralLedgerManagerAggregateService>(
      GeneralLedgerManagerAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
