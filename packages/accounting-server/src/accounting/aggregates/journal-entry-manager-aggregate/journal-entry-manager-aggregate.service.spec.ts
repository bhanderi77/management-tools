import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryManagerAggregateService } from './journal-entry-manager-aggregate.service';
import { JournalEntryValidationPolicy } from '../../policies/journal-entry-validation-policy/journal-entry-validation-policy.service';
import { GenerateMetaDataPolicy } from '../../policies/generate-meta-data/generate-meta-data.service-policy';

describe('JournalEntryManagerAggregateService', () => {
  let service: JournalEntryManagerAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryManagerAggregateService,
        {
          provide: JournalEntryValidationPolicy,
          useValue: {},
        },
        {
          provide: GenerateMetaDataPolicy,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<JournalEntryManagerAggregateService>(
      JournalEntryManagerAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
