export const SEND_EMAIL = 'send_email';
export const AUTHORIZATION = 'authorization';
export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const ACCOUNT_TYPES = [
  'ASSET',
  'LIABILITY',
  'INCOME',
  'EXPENSE',
  'EQUITY',
];
export const ASSET = 'ASSET';
export const LIABILITY = 'LIABILITY';
export const INCOME = 'INCOME';
export const EXPENSE = 'EXPENSE';
export const EQUITY = 'EQUITY';
export const PERIOD_CLOSING = 'period_closing';
export const MONTHLY = 'monthly';
export const QUARTERLY = 'quarterly';
export const HALF_YEARLY = 'half yearly';
export const YEARLY = 'yearly';
export const CREDIT = 'CREDIT';
export const DEBIT = 'DEBIT';
export const PROFIT_ACCOUNTS = ['400000000', '500000000'];
export const INCOME_ACCOUNT_NUMBER = '400000000';
export const BALANCE_REPORT_ACCOUNT = ['ASSET', 'LIABILITY', 'EQUITY'];
export const EXPENSE_ACCOUNT_NUMBER = '500000000';
export const EXPENDITURE_REPORT_ACCOUNT = ['INCOME', 'EXPENSE'];
export const BULK_JSON = 'JSON';
export const BULK_CSV = 'CSV';
