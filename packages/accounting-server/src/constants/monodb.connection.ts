import { ConfigService } from '../config/config.service';
import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import { Settings } from '../system-settings/settings/settings.collection';
import { TokenCache } from '../auth/entities/token-cache/token-cache.collection';

const config = new ConfigService();

export const MONGODB_CONNECTION_NAME = 'mongodb';

export const MONGODB_CONNECTION: MongoConnectionOptions = {
  name: MONGODB_CONNECTION_NAME,
  type: 'mongodb',
  host: config.get('MONGODB_HOST'),
  database: config.get('MONGODB_NAME'),
  username: config.get('MONGODB_USER'),
  password: config.get('MONGODB_PASSWORD'),
  logging: false,
  entities: [Settings, TokenCache],
  useNewUrlParser: true,
  synchronize: true,
};
