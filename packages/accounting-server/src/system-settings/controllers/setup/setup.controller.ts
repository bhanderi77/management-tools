import {
  Post,
  Controller,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SetupService } from './setup.service';
import { SettingsDto } from '../../../system-settings/settings/settings.dto';

@Controller('setup')
export class SetupController {
  constructor(private readonly settingsService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() accSettingsDTO: SettingsDto) {
    return await this.settingsService.setup(accSettingsDTO);
  }
}
