import { Injectable, HttpService } from '@nestjs/common';
import { settingsAlreadyExists, somethingWentWrong } from '../../../exceptions';
import { Settings } from '../../../system-settings/settings/settings.collection';
import { SettingsService } from '../../../system-settings/settings/settings.service';

@Injectable()
export class SetupService {
  protected accSettings: Settings;

  constructor(
    protected readonly settingsService: SettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.settingsService.count()) {
      throw settingsAlreadyExists;
    }
    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .subscribe({
        next: async response => {
          params.authorizationURL = response.data.authorization_endpoint;
          params.tokenURL = response.data.token_endpoint;
          params.profileURL = response.data.userinfo_endpoint;
          params.revocationURL = response.data.revocation_endpoint;
          params.introspectionURL = response.data.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          this.accSettings = await this.settingsService.save(params);
          return this.accSettings;
        },
        error: error => {
          // TODO : meaningful errors
          throw somethingWentWrong;
        },
      });
  }

  async getInfo() {
    const info = await this.settingsService.find();
    if (info) {
      delete info.clientSecret, info._id;
    }
    return info;
  }
}
