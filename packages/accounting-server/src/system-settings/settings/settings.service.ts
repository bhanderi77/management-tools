import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Settings } from './settings.collection';
import { settingsAlreadyExists } from '../../exceptions';
import { MONGODB_CONNECTION_NAME } from '../../constants/monodb.connection';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(Settings, MONGODB_CONNECTION_NAME)
    private readonly settingsRepository: MongoRepository<Settings>,
  ) {}

  async save(params) {
    let serverSettings = new Settings();
    if (params.uuid) {
      const exists: number = await this.count();
      serverSettings = await this.findOne({ uuid: params.uuid });
      serverSettings.appURL = params.appURL;
      if (exists > 0 && !serverSettings) {
        throw settingsAlreadyExists;
      }
      serverSettings.save();
    } else {
      Object.assign(serverSettings, params);
    }
    return await this.settingsRepository.save(serverSettings);
  }

  async findAll() {
    const allSettings = await this.settingsRepository.find();
    return allSettings;
  }

  async find(): Promise<Settings> {
    const settings = await this.settingsRepository.find();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.settingsRepository.findOne(params);
  }

  async update(query, params) {
    return await this.settingsRepository.update(query, params);
  }

  async count() {
    return this.settingsRepository.count();
  }

  async setPeriodClosing(periodClosing) {
    const settings = await this.find();
    settings.periodClosing = periodClosing;
    return await this.settingsRepository.save(settings);
  }
}
