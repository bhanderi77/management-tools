import { GetCustomerEnquiryQueryHandler } from './customer-enquiry/customer-enquiry.handler';

export const QueriesHandlers = [GetCustomerEnquiryQueryHandler];
