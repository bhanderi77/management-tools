import { AggregateRoot } from '@nestjs/cqrs';
import * as uuid from 'uuidv4';
import { Enquiry } from '../../entities/customer-enquiry/customer-enquiry.collection';
import { CustomerEnquiryCreatedEvent } from '../../events/customer-enquiry-created/customer-enquiry-created.event';

export class CustomerEnquiryAggregateManager extends AggregateRoot {
  constructor() {
    super();
  }

  createNewCustomerInquiry(clientFormRequest) {
    clientFormRequest.uuid = uuid();
    const provider = Object.assign(new Enquiry(), clientFormRequest);
    return this.apply(new CustomerEnquiryCreatedEvent(provider));
  }
}
