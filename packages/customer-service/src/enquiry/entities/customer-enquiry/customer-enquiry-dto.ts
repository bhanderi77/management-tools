import { Entity } from 'typeorm';
import { IsString, IsUrl, IsNumber, IsNotEmpty, IsDate } from 'class-validator';

@Entity()
export class EnquiryDTO {
  uuid?: string;

  @IsString()
  @IsDate()
  date: Date;

  @IsString()
  @IsNotEmpty()
  createdBy: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsUrl()
  @IsNotEmpty()
  email: string;

  @IsNumber()
  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  organizationType: string;

  @IsString()
  @IsNotEmpty()
  team: string;

  @IsString()
  @IsNotEmpty()
  queryType: string;

  @IsString()
  @IsNotEmpty()
  priority: string;

  @IsString()
  @IsNotEmpty()
  queryBrief: string;
}
