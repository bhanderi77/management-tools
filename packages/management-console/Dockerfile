FROM node:latest
# Copy and build server
COPY packages/management-console /home/craft/management-console
WORKDIR /home/craft/
RUN cd management-console \
    && rm -fr .env \
    && npm install \
    && npm run server:build \
    && rm -fr node_modules

FROM node:slim
# Install Dockerize
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Setup docker-entrypoint
COPY packages/management-console/docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

# Add non root user and set home directory
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/management-console
COPY --from=0 /home/craft/management-console .
RUN npm install --only=production

# Copy Docker files and config
COPY packages/management-console/docker ./docker/
RUN chown -R craft:craft /home/craft

# Expose port
EXPOSE 9100

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
