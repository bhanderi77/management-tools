import { Module, Global, HttpModule } from '@nestjs/common';
import { TokenCacheService } from './entities/token-cache/token-cache.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  TOKEN_CACHE,
  tokenCacheSchema,
} from './entities/token-cache/token-cache.schema';
import { RoleGuard } from './guards/role.guard';
import { TokenGuard } from './guards/token.guard';
import { CheckGuard } from './guards/check.guard';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: TOKEN_CACHE, schema: tokenCacheSchema },
    ]),
    HttpModule,
  ],
  providers: [TokenCacheService, RoleGuard, TokenGuard, CheckGuard],
  exports: [TokenCacheService, RoleGuard, TokenGuard, CheckGuard, HttpModule],
})
export class AuthModule {}
