import {
  CanActivate,
  ExecutionContext,
  Injectable,
  BadRequestException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { INVALID_CLIENT } from '../../constants/app-strings';

@Injectable()
export class CheckGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const client = context.switchToWs().getClient();
    if (client) {
      return true;
    } else {
      throw new BadRequestException(INVALID_CLIENT);
    }
  }
}
