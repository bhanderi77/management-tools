import { Document } from 'mongoose';

export interface TokenCacheInterface extends Document {
  accessToken?: string;
  uuid?: string;
  active?: boolean;
  exp?: number;
  sub?: string;
  scope?: any;
  roles?: any;
  clientId?: string;
}
