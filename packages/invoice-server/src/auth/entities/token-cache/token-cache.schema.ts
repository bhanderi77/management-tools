import * as mongoose from 'mongoose';
import * as uuidv4 from 'uuid/v4';

const schema = new mongoose.Schema(
  {
    _id: { type: String, default: uuidv4 },
    accessToken: String,
    uuid: String,
    active: Boolean,
    exp: Number,
    sub: String,
    scope: JSON,
    roles: JSON,
    clientId: String,
  },
  { collection: 'token_cache', versionKey: false },
);

export const tokenCacheSchema = schema;

export const TOKEN_CACHE = 'TokenCache';

export const TokenCacheModel = mongoose.model(TOKEN_CACHE, tokenCacheSchema);
