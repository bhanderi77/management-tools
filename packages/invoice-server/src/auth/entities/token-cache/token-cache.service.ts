import { Injectable } from '@nestjs/common';
import { TokenCacheInterface } from './token-cache.interface';
import { InjectModel } from '@nestjs/mongoose';
import { TOKEN_CACHE } from './token-cache.schema';
import { Model } from 'mongoose';

@Injectable()
export class TokenCacheService {
  constructor(
    @InjectModel(TOKEN_CACHE)
    private readonly tokenCacheRepository: Model<TokenCacheInterface>,
  ) {}

  async save(params) {
    const tokenCache = new this.tokenCacheRepository();
    Object.assign(tokenCache, params);
    return await tokenCache.save();
  }

  async find(): Promise<TokenCacheInterface[]> {
    return await this.tokenCacheRepository.find();
  }

  async findOne(params) {
    return await this.tokenCacheRepository.findOne(params);
  }

  async update(query, params) {
    return await this.tokenCacheRepository.update(query, params);
  }

  async count() {
    return await this.tokenCacheRepository.estimatedDocumentCount();
  }

  async paginate(skip: number, take: number) {
    return await this.tokenCacheRepository.find({ skip, take });
  }

  async deleteMany(params) {
    return await this.tokenCacheRepository.deleteMany(params);
  }
}
