import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { PurchaseInvoiceUpdatedEvent } from './purchase-invoice-updated.event';
import { PurchaseInvoiceService } from '../../entities/purchase-invoice/purchase-invoice.service';

@EventsHandler(PurchaseInvoiceUpdatedEvent)
export class PurchaseInvoiceUpdatedHandler
  implements IEventHandler<PurchaseInvoiceUpdatedEvent> {
  constructor(
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
  ) {}
  async handle(event: PurchaseInvoiceUpdatedEvent) {
    const purchaseInvoicePayload = event.purchaseInvoiceData;
    const purchaseInvoiceModel = await this.purchaseInvoiceService.findOne(
      purchaseInvoicePayload.uuid,
    );
    delete purchaseInvoicePayload.invoiceNumber;
    Object.assign(purchaseInvoiceModel, purchaseInvoicePayload);
    await purchaseInvoiceModel.save();
  }
}
