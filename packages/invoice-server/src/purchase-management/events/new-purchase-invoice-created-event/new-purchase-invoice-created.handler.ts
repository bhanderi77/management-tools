import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { NewPurchaseInvoiceCreatedEvent } from './new-purchase-invoice-created.event';
import { PurchaseInvoiceService } from '../../entities/purchase-invoice/purchase-invoice.service';
import { PurchaseInvoice } from '../../entities/purchase-invoice/purchase-invoice.interface';

@EventsHandler(NewPurchaseInvoiceCreatedEvent)
export class NewPurchaseInvoiceCreatedHandler
  implements IEventHandler<NewPurchaseInvoiceCreatedEvent> {
  constructor(
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
  ) {}
  async handle(event: NewPurchaseInvoiceCreatedEvent) {
    const clientHttpRequest = event.clientHttpRequest;
    const createdBy = clientHttpRequest.token.sub;
    const purchaseInvoicePayload = event.purchaseInvoiceData;

    const purchaseInvoiceModel = this.purchaseInvoiceService.getModel();
    const newPurchaseInvoice: PurchaseInvoice = new purchaseInvoiceModel();
    newPurchaseInvoice.uuid = purchaseInvoicePayload.uuid;
    newPurchaseInvoice.date = purchaseInvoicePayload.date;
    newPurchaseInvoice.invoiceItems = purchaseInvoicePayload.invoiceItems;
    newPurchaseInvoice.createdBy = createdBy;
    newPurchaseInvoice.invoiceName = purchaseInvoicePayload.invoiceName;
    newPurchaseInvoice.customerId = purchaseInvoicePayload.customerId;
    newPurchaseInvoice.customerName = purchaseInvoicePayload.customerName;
    newPurchaseInvoice.invoiceNumber = purchaseInvoicePayload.invoiceNumber;
    newPurchaseInvoice.tax = purchaseInvoicePayload.tax;
    newPurchaseInvoice.termsAndConditions =
      purchaseInvoicePayload.termsAndConditions;
    await this.purchaseInvoiceService.save(newPurchaseInvoice);
  }
}
