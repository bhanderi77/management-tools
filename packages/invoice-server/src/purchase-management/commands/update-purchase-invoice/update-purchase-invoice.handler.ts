import { UpdatePurchaseInvoiceCommand } from './update-purchase-invoice.command';
import { PurchaseInvoiceManagerAggregate } from '../../../purchase-management/aggregates';
import { EventPublisher, ICommandHandler, CommandHandler } from '@nestjs/cqrs';

@CommandHandler(UpdatePurchaseInvoiceCommand)
export class UpdatePurchaseInvoiceHandler
  implements ICommandHandler<UpdatePurchaseInvoiceCommand> {
  constructor(
    private readonly manager: PurchaseInvoiceManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdatePurchaseInvoiceCommand) {
    const {
      purchaseInvoiceData: purchaseInvoiceData,
      clientHttpRequest,
    } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updatePurchaseInvocie(
      purchaseInvoiceData,
      clientHttpRequest,
    );
    aggregate.commit();
  }
}
