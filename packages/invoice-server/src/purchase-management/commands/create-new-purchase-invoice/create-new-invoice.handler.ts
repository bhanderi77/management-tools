import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateNewPurchaseInvoiceCommand } from './create-new-invoice.command';
import { PurchaseInvoiceManagerAggregate } from '../../../purchase-management/aggregates/';

@CommandHandler(CreateNewPurchaseInvoiceCommand)
export class CreateNewPurchaseInvoiceHandler
  implements ICommandHandler<CreateNewPurchaseInvoiceCommand> {
  constructor(
    private readonly manager: PurchaseInvoiceManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateNewPurchaseInvoiceCommand) {
    const {
      purchaseInvoiceData: purchaseInvoiceData,
      clientHttpRequest,
    } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.createNewInvoice(purchaseInvoiceData, clientHttpRequest);
    aggregate.commit();
  }
}
