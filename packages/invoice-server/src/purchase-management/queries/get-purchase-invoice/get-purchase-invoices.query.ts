import { IQuery } from '@nestjs/cqrs';

export class GetPurchaseInvoicesQuery implements IQuery {
  constructor(public uuid: string) {}
}
