import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetPurchaseInvoicesQuery } from './get-purchase-invoices.query';
import { PurchaseInvoiceService } from '../../entities/purchase-invoice/purchase-invoice.service';

@QueryHandler(GetPurchaseInvoicesQuery)
export class GetPurchaseInvoicesHandler
  implements IQueryHandler<GetPurchaseInvoicesQuery> {
  constructor(
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
  ) {}
  async execute(query: GetPurchaseInvoicesQuery) {
    return await this.purchaseInvoiceService.findOne({
      uuid: query.uuid,
    });
  }
}
