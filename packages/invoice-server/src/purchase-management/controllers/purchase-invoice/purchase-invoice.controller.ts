import {
  Controller,
  Post,
  Body,
  Req,
  Get,
  Query,
  UseGuards,
  Param,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateNewPurchaseInvoiceCommand } from '../../../purchase-management/commands/create-new-purchase-invoice/create-new-invoice.command';
import { PurchaseInvoiceDto } from './purchase-invoice-dto';
import { ListPurchaseInvoicesQuery } from '../../queries/list-purchase-invoices/list-purchase-invoices.query';
import { PurchaseInvoiceService } from '../../entities/purchase-invoice/purchase-invoice.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { GetPurchaseInvoicesQuery } from '../../queries/get-purchase-invoice/get-purchase-invoices.query';
import { UpdatePurchaseInvoiceCommand } from '../../commands/update-purchase-invoice/update-purchase-invoice.command';

@Controller('purchase_invoice')
export class PurchaseInvoiceController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly purchaseInvoiceService: PurchaseInvoiceService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(ValidationPipe)
  async createNewPurchaseInvoice(
    @Body() payload: PurchaseInvoiceDto,
    @Req() req,
  ) {
    return await this.commandBus.execute(
      new CreateNewPurchaseInvoiceCommand(payload, req),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(ValidationPipe)
  async updatePurchaseInvoice(@Body() payload: PurchaseInvoiceDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdatePurchaseInvoiceCommand(payload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getPurchaseInvoice(@Param('uuid') uuid: string) {
    return await this.queryBus.execute(new GetPurchaseInvoicesQuery(uuid));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getPurchaseInvoiceList(
    @Req() req,
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // query.createdBy = req.user.user;

    const sortQuery = { name: sort };
    return await this.queryBus.execute(
      new ListPurchaseInvoicesQuery(
        this.purchaseInvoiceService.getModel(),
        offset,
        limit,
        search,
        query,
        ['name', 'clientId'],
        sortQuery,
      ),
    );
  }
}
