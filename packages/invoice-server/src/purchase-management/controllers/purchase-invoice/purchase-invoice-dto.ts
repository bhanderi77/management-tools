import {
  IsString,
  IsNotEmpty,
  IsDate,
  IsOptional,
  IsArray,
} from 'class-validator';

export class PurchaseInvoiceDto {
  @IsString()
  @IsOptional()
  uuid: string;

  @IsString()
  @IsNotEmpty()
  invoiceName;

  @IsString()
  @IsNotEmpty()
  customerId;

  @IsString()
  @IsNotEmpty()
  customerName;

  @IsString()
  @IsDate()
  date;

  @IsString()
  @IsNotEmpty()
  invoiceNumber;

  @IsArray()
  @IsOptional()
  invoiceItems;

  @IsString()
  @IsNotEmpty()
  tax;

  @IsString()
  @IsNotEmpty()
  termsAndConditions;
}
