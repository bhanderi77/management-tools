import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { PurchaseInvoice } from './purchase-invoice.interface';
import { PURCHASE_INVOICE } from './purchase-invoice.schema';

@Injectable()
export class PurchaseInvoiceService {
  constructor(
    @InjectModel(PURCHASE_INVOICE)
    private readonly invoiceModel: Model<PurchaseInvoice>,
  ) {}

  async save(params) {
    const createdPurchaseInvoice = new this.invoiceModel(params);
    return await createdPurchaseInvoice.save();
  }

  async findAll(): Promise<PurchaseInvoice[]> {
    return await this.invoiceModel.find().exec();
  }

  async findOne(param) {
    return await this.invoiceModel.findOne(param);
  }

  getModel() {
    return this.invoiceModel;
  }
}
