import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    uuid: { type: String },
    date: { type: Date },
    createdBy: { type: String },
    invoiceName: { type: String },
    customerId: { type: String },
    customerName: { type: String },
    invoiceNumber: { type: String, unique: true },
    invoiceItems: { type: [] },
    tax: { type: String },
    termsAndConditions: { type: String },
  },
  { collection: 'purchase_invoice', versionKey: false },
);

export const PurchaseInvoiceSchema = schema;

export const PURCHASE_INVOICE = 'PurchaseInvoice';

export const PurchaseInvoiceModel = mongoose.model(
  PURCHASE_INVOICE,
  PurchaseInvoiceSchema,
);
