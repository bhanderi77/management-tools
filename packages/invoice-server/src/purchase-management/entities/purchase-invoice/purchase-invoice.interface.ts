import { Document } from 'mongoose';

export interface PurchaseInvoice extends Document {
  uuid?: string;
  date?: Date;
  createdBy?: string;
  invoiceName?: string;
  customerId?: string;
  customerName?: string;
  invoiceNumber?: string;
  tax?: string;
  invoiceItems?: number;
  termsAndConditions?: string;
}
