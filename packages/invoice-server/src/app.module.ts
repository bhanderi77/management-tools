import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PurchaseInvoiceModule } from './purchase-management/purchase-invoice-module';
import { SalesInvoiceModule } from './sales-management/sales-invoice-module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { InvoiceSettingsModule } from './system-settings/invoice-settings.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    PurchaseInvoiceModule,
    SalesInvoiceModule,
    AuthModule,
    ConfigModule,
    InvoiceSettingsModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        const uri = `mongodb://${config.get('DB_USER')}:${config.get(
          'DB_PASSWORD',
        )}@${config.get('DB_HOST')}/${config.get('DB_NAME')}`;
        return {
          uri,
          useNewUrlParser: true,
          useCreateIndex: true,
        };
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
