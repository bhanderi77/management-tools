import { Test, TestingModule } from '@nestjs/testing';
import { SubmitSalesInvoicePolicy } from './submit-sales-invoice-policy.service';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/settings/settings.service';

describe('SubmitSalesInvoicepolicy', () => {
  let service: SubmitSalesInvoicePolicy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SubmitSalesInvoicePolicy,
        {
          provide: SalesInvoiceService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SubmitSalesInvoicePolicy>(SubmitSalesInvoicePolicy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
