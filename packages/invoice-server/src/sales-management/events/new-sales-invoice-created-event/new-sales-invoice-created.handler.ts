import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { NewSalesInvoiceCreatedEvent } from './new-sales-invoice-created.event';
import { SalesInvoice } from '../../entities/sales-invoice/sales-invoice.interface';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';

@EventsHandler(NewSalesInvoiceCreatedEvent)
export class NewSalesInvoiceCreatedHandler
  implements IEventHandler<NewSalesInvoiceCreatedEvent> {
  constructor(private readonly salesInvoiceService: SalesInvoiceService) {}
  async handle(event: NewSalesInvoiceCreatedEvent) {
    const clientHttpRequest = event.clientHttpRequest;
    const createdBy = clientHttpRequest.token.sub;
    const salesInvoicePayload = event.salesInvoiceData;

    const salesInvoiceModel = this.salesInvoiceService.getModel();
    const newsalesInvoice: SalesInvoice = new salesInvoiceModel();
    newsalesInvoice.uuid = salesInvoicePayload.uuid;
    newsalesInvoice.date = salesInvoicePayload.date;
    newsalesInvoice.createdBy = createdBy;
    newsalesInvoice.invoiceName = salesInvoicePayload.invoiceName;
    newsalesInvoice.customerId = salesInvoicePayload.customerId;
    newsalesInvoice.customerName = salesInvoicePayload.customerName;
    newsalesInvoice.invoiceNumber = salesInvoicePayload.invoiceNumber;
    newsalesInvoice.termsAndConditions = salesInvoicePayload.termsAndConditions;
    newsalesInvoice.invoiceItems = salesInvoicePayload.invoiceItems;
    newsalesInvoice.tax = salesInvoicePayload.tax;
    await this.salesInvoiceService.save(newsalesInvoice);
  }
}
