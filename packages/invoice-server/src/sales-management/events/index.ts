import { NewSalesInvoiceCreatedHandler } from './new-sales-invoice-created-event/new-sales-invoice-created.handler';
import { SalesInvoiceUpdatedHandler } from './sales-invoice-updated-event/sales-invoice-updated.handler';
import { SalesInvoiceSubmittedHandler } from './sales-invoice-submitted-event/sales-invoice-submitted.handler';

export const SalesInvoiceEventManager = [
  NewSalesInvoiceCreatedHandler,
  SalesInvoiceUpdatedHandler,
  SalesInvoiceSubmittedHandler,
];
