import { IEvent } from '@nestjs/cqrs';
import { SalesInvoiceDto } from '../../controllers/sales-invoice/sales-invoice-dto';

export class SalesInvoiceUpdatedEvent implements IEvent {
  constructor(
    public salesInvoiceData: SalesInvoiceDto,
    public clientHttpRequest: any,
  ) {}
}
