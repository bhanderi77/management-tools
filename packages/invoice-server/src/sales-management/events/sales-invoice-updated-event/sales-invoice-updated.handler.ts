import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SalesInvoiceUpdatedEvent } from './sales-invoice-updated.event';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';

@EventsHandler(SalesInvoiceUpdatedEvent)
export class SalesInvoiceUpdatedHandler
  implements IEventHandler<SalesInvoiceUpdatedEvent> {
  constructor(private readonly salesInvoiceService: SalesInvoiceService) {}
  async handle(event: SalesInvoiceUpdatedEvent) {
    const salesInvoicePayload = event.salesInvoiceData;
    const salesInvoiceModel = await this.salesInvoiceService.findOne(
      salesInvoicePayload.uuid,
    );
    delete salesInvoicePayload.invoiceNumber;
    Object.assign(salesInvoiceModel, salesInvoicePayload);
    await salesInvoiceModel.save();
  }
}
