import { AggregateRoot } from '@nestjs/cqrs';
import { NewSalesInvoiceCreatedEvent } from '../../events/new-sales-invoice-created-event/new-sales-invoice-created.event';
import * as uuidv4 from 'uuid/v4';
import { SalesInvoiceUpdatedEvent } from '../../events/sales-invoice-updated-event/sales-invoice-updated.event';
import { SubmitSalesInvoicePolicy } from '../../policies/submit-sales-invoice-policy/submit-sales-invoice-policy.service';
import { Injectable } from '@nestjs/common';
import { SalesInvoiceSubmittedEvent } from '../../events/sales-invoice-submitted-event/sales-invoice-submitted.event';
import { map, mergeMap, toArray } from 'rxjs/operators';
import { of, from } from 'rxjs';

@Injectable()
export class SalesInvoiceManagerAggregate extends AggregateRoot {
  constructor(private readonly salesInvoicePolicy: SubmitSalesInvoicePolicy) {
    super();
  }

  createNewInvoice(salesInvoiceData, clientHttpRequest) {
    return from(salesInvoiceData.invoiceItems)
      .pipe(
        mergeMap((item: any) => {
          return this.salesInvoicePolicy.validateInvoiceItems(
            item,
            clientHttpRequest,
          );
        }),
        mergeMap(data => {
          return of(data);
        }),
        toArray(),
      )
      .pipe(
        map(data => {
          const uuid = uuidv4();
          salesInvoiceData.uuid = uuid;
          return this.apply(
            new NewSalesInvoiceCreatedEvent(
              salesInvoiceData,
              clientHttpRequest,
            ),
          );
        }),
      );
  }

  async updateSalesInvoice(salesInvoiceData, clientHttpRequest) {
    return this.apply(
      new SalesInvoiceUpdatedEvent(salesInvoiceData, clientHttpRequest),
    );
  }

  submitSalesInvoice(uuid, clientHttpRequest) {
    return this.salesInvoicePolicy
      .validateSalesInvoice(uuid, clientHttpRequest)
      .pipe(
        map(response => {
          if (response) {
            return this.apply(
              new SalesInvoiceSubmittedEvent(response, clientHttpRequest),
            );
          }
        }),
      );
  }
}
