import { Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { SalesInvoiceAggregate } from './aggregates';
import { SalesInvoiceEventManager } from './events';
import { SalesInvoiceCommandManger } from './commands';
import { SalesInvoiceController } from './controllers/sales-invoice/sales-invoice.controller';
import { SalesInvoiceEntitiesModule } from './entities/sales-entities.module';
import { SalesManagementQueries } from './queries';
import { SalesInvoicePolicies } from './policies';

@Global()
@Module({
  imports: [CqrsModule, SalesInvoiceEntitiesModule],
  controllers: [SalesInvoiceController],
  providers: [
    ...SalesInvoiceAggregate,
    ...SalesInvoiceEventManager,
    ...SalesInvoiceCommandManger,
    ...SalesManagementQueries,
    ...SalesInvoicePolicies,
  ],
  exports: [SalesInvoiceEntitiesModule],
})
export class SalesInvoiceModule {}
