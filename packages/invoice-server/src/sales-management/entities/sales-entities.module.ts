import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  SALES_INVOICE,
  SalesInvoiceSchema,
} from './sales-invoice/sales-invoice.schema';
import { SalesInvoiceService } from './sales-invoice/sales-invoice.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SALES_INVOICE, schema: SalesInvoiceSchema },
    ]),
  ],
  providers: [SalesInvoiceService],
  exports: [SalesInvoiceService],
})
export class SalesInvoiceEntitiesModule {}
