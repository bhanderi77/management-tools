import { Test, TestingModule } from '@nestjs/testing';
import { SalesInvoiceService } from './sales-invoice.service';
import { getModelToken } from '@nestjs/mongoose';
import { SALES_INVOICE } from './sales-invoice.schema';

describe('SalesInvoiceService', () => {
  let service: SalesInvoiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SalesInvoiceService,
        {
          provide: getModelToken(SALES_INVOICE),
          useValue: {}, // use mock values
        },
      ],
    }).compile();

    service = module.get<SalesInvoiceService>(SalesInvoiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
