import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    uuid: { type: String },
    date: { type: Date },
    createdBy: { type: String },
    invoiceName: { type: String },
    customerId: { type: String },
    customerName: { type: String },
    invoiceNumber: { type: String, unique: true },
    invoiceItems: { type: [] },
    termsAndConditions: { type: String },
    tax: { type: String },
  },
  { collection: 'sales_invoice', versionKey: false },
);

export const SalesInvoiceSchema = schema;

export const SALES_INVOICE = 'SalesInvoice';

export const SalesInvoiceModel = mongoose.model(
  SALES_INVOICE,
  SalesInvoiceSchema,
);
