import { EventPublisher, ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { SubmitSalesInvoiceCommand } from './submit-sales-invoice.command';
import { SalesInvoiceManagerAggregate } from '../../aggregates/sales-invoice-manager-aggregate/sales-invoice-manager-aggregate';

@CommandHandler(SubmitSalesInvoiceCommand)
export class SubmitSalesInvoiceHandler
  implements ICommandHandler<SubmitSalesInvoiceCommand> {
  constructor(
    private readonly manager: SalesInvoiceManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: SubmitSalesInvoiceCommand) {
    const { uuid, clientHttpRequest } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.submitSalesInvoice(uuid, clientHttpRequest).toPromise();
    aggregate.commit();
  }
}
