import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateNewSalesInvoiceCommand } from './create-new-sales-invoice.command';
import { SalesInvoiceManagerAggregate } from '../../../sales-management/aggregates/sales-invoice-manager-aggregate/sales-invoice-manager-aggregate';

@CommandHandler(CreateNewSalesInvoiceCommand)
export class CreateNewSalesInvoiceHandler
  implements ICommandHandler<CreateNewSalesInvoiceCommand> {
  constructor(
    private readonly manager: SalesInvoiceManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateNewSalesInvoiceCommand) {
    const { salesInvoiceData, clientHttpRequest } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager
      .createNewInvoice(salesInvoiceData, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
