import { CreateNewSalesInvoiceHandler } from './create-new-sales-invoice/create-new-sales-invoice.handler';
import { UpdateSalesInvoiceHandler } from './update-sales-invoice/update-sales-invoice.handler';
import { SubmitSalesInvoiceHandler } from './submit-sales-invoice/submit-sales-invoice.handler';

export const SalesInvoiceCommandManger = [
  CreateNewSalesInvoiceHandler,
  UpdateSalesInvoiceHandler,
  SubmitSalesInvoiceHandler,
];
