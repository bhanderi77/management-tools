import { IQuery } from '@nestjs/cqrs';
import { Model } from 'mongoose';
import { SalesInvoice } from '../../entities/sales-invoice/sales-invoice.interface';

export class ListSalesInvoicesQuery implements IQuery {
  constructor(
    public model: Model<SalesInvoice>,
    public offset: number,
    public limit: number,
    public search: string,
    public query: { createdBy?: string },
    public searchField: string[],
    public sortQuery: { name: string },
  ) {}
}
