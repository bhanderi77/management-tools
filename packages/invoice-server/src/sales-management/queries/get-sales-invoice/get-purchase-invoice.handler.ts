import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';
import { GetSalesInvoicesQuery } from './get-purchase-invoices.query';

@QueryHandler(GetSalesInvoicesQuery)
export class GetsalesInvoicesHandler
  implements IQueryHandler<GetSalesInvoicesQuery> {
  constructor(private readonly salesInvoiceService: SalesInvoiceService) {}
  async execute(query: GetSalesInvoicesQuery) {
    return await this.salesInvoiceService.findOne({
      uuid: query.uuid,
    });
  }
}
