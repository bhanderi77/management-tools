import { ListSalesInvoicesHandler } from './list-sales-invoices/list-sales-invoices.handles';
import { GetsalesInvoicesHandler } from './get-sales-invoice/get-purchase-invoice.handler';

export const SalesManagementQueries = [
  ListSalesInvoicesHandler,
  GetsalesInvoicesHandler,
];
