import { Module, Global } from '@nestjs/common';
import { SettingsService } from './settings/settings.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SETTINGS, settingsSchema } from './settings/settings.schema';
import { ConnectController } from './controllers/connect/connect.controller';
import { SettingsController } from './controllers/settings/settings.controller';
import { SetupController } from './controllers/setup/setup.controller';
import { SetupService } from './controllers/setup/setup.service';
import { SettingsAggregates } from './aggregates';
import { SettingEventManager } from './event';
import { SettingsCommandManager } from './command';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([{ name: SETTINGS, schema: settingsSchema }]),
  ],
  controllers: [ConnectController, SettingsController, SetupController],
  providers: [
    SettingsService,
    SetupService,
    ...SettingsAggregates,
    ...SettingEventManager,
    ...SettingsCommandManager,
  ],
  exports: [SettingsService, SetupService],
})
export class InvoiceSettingsModule {}
