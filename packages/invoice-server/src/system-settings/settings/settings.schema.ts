import * as mongoose from 'mongoose';
import * as uuidv4 from 'uuid/v4';

const schema = new mongoose.Schema(
  {
    _id: { type: String, default: uuidv4() },
    uuid: String,
    appURL: String,
    authServerURL: String,
    clientId: String,
    clientSecret: String,
    profileURL: String,
    tokenURL: String,
    introspectionURL: String,
    authorizationURL: String,
    callbackURLs: [String],
    revocationURL: String,
    accountingServer: String,
  },
  { collection: 'settings', versionKey: false },
);

export const settingsSchema = schema;

export const SETTINGS = 'Settings';

export const SettingsModel = mongoose.model(SETTINGS, settingsSchema);
