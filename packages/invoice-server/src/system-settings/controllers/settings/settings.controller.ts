import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  Put,
  Query,
} from '@nestjs/common';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ACCOUNTANT } from '../../../constants/roles';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { SetupAccountingServerCommand } from '../../command/setup-accouting-server/setup-accouting-server.command';
import { CommandBus } from '@nestjs/cqrs';
import { SettingsDto } from '../../settings/settings.dto';

@Controller('settings')
export class SettingsController {
  constructor(
    private readonly settingsService: SettingsService,
    private readonly commandBus: CommandBus,
  ) {}

  @Get('v1/getSettings')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async getSettings() {
    return await this.settingsService.findAll();
  }

  @Put('v1/update')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async updateSettings(@Body('settings') settings, @Query('query') query) {
    return await this.settingsService.update(query, settings);
  }

  @Post('v1/setup_accounting_server')
  @UseGuards(TokenGuard, RoleGuard)
  async setupAccountingServer(@Body() settings) {
    return await this.commandBus.execute(
      new SetupAccountingServerCommand(settings),
    );
  }

  @Post('v1/create')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async createSettings(@Body('settings') settings: SettingsDto) {
    return await this.settingsService.save(settings);
  }
}
