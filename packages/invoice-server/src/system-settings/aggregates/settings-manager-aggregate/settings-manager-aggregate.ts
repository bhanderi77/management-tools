import { AggregateRoot } from '@nestjs/cqrs';
import { AccountingServerSetupCompleteEvent } from '../../event/accounting-server-setup-complete/accounting-server-setup-complete.event';

export class SettingsManagerAggregate extends AggregateRoot {
  constructor() {
    super();
  }

  async SetupAccountingServer(settingsData) {
    return this.apply(new AccountingServerSetupCompleteEvent(settingsData));
  }
}
