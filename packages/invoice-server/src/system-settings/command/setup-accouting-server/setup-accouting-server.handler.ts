import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { SetupAccountingServerCommand } from './setup-accouting-server.command';
import { SettingsManagerAggregate } from '../../aggregates/settings-manager-aggregate/settings-manager-aggregate';

@CommandHandler(SetupAccountingServerCommand)
export class SetupAccountingServerHandler
  implements ICommandHandler<SetupAccountingServerCommand> {
  constructor(
    private readonly manager: SettingsManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: SetupAccountingServerCommand) {
    const { settingsData } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.SetupAccountingServer(settingsData);
    aggregate.commit();
  }
}
