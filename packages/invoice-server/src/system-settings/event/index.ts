import { AccountingServerSetupCompleteHandler } from './accounting-server-setup-complete/accounting-server-setup-complete.handler';

export const SettingEventManager = [AccountingServerSetupCompleteHandler];
