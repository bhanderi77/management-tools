import { IEvent } from '@nestjs/cqrs';
import { SettingsDto } from '../../settings/settings.dto';

export class AccountingServerSetupCompleteEvent implements IEvent {
  constructor(public settingsData: SettingsDto) {}
}
