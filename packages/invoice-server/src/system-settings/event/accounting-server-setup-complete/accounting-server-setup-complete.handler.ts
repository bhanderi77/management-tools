import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountingServerSetupCompleteEvent } from './accounting-server-setup-complete.event';
import { SettingsService } from '../../settings/settings.service';

@EventsHandler(AccountingServerSetupCompleteEvent)
export class AccountingServerSetupCompleteHandler
  implements IEventHandler<AccountingServerSetupCompleteEvent> {
  constructor(private readonly settingService: SettingsService) {}
  async handle(event: AccountingServerSetupCompleteEvent) {
    const settings: any = await this.settingService.find();
    settings.accountingServer = event.settingsData.accountingServer;
    return await this.settingService.save(settings);
  }
}
